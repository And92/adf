package andrea.baroni;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
//import java.net.JarURLConnection;
//import java.net.URI;
import java.net.URISyntaxException;
//import java.net.URL;
import java.util.ArrayList;
//import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;



/*import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;*/




//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.stream.StreamSource;
import net.sf.saxon.TransformerFactoryImpl;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
/*import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;*/

public class DOCX2ADF {

	static String XSLT_FILE = "xslt/fromdocx2adf.xsl";
	static String XML_FILE = "document.xml";
	
	//static String input_fld = "test/";
	static String INPUT_FILE; // = "doceng15_short_v6.odt";
	static String OUTPUT_FLD; // = "test/out/";
	static String OUTPUT_FILE;
	
	static String tempFolderName;
	static boolean Content = false;
	static boolean ToPages = false;
	
	private static final Logger log = Logger.getLogger(DOCX2ADF.class.getName());
	
	public static void main(String[] args) throws IOException, IllegalArgumentException, URISyntaxException {
		
		// DISABLE LOGGING TO STDOUT
		//log.setUseParentHandlers(false);
		
		Options options = new Options();
		options.addOption("i", true, "specify the input file");
		options.addOption("o", true, "specify the output folder");
		options.addOption("h", false, "print this message");
		options.addOption("content", false, "for convert only content without FrontMatter, CoverPage, To*");
		options.addOption("topages", false, "for convert only content without FrontMatter, CoverPage, To* and display empty span, for unrecognized image file format");
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("h")) {
				System.out.println("-i  <file>		MANDATORY - THe input .docx document.\n");
				System.out.println("-o  <folder>	MANDATORY - The folder where the output ADF file should be stored.\n");
				System.out.println("-content  OPTIONAL - for convert only content without FrontMatter, CoverPage, To*\n");
				System.out.println("-topages OPTIONAL - for convert only content without FrontMatter, CoverPage, To* and display empty span, for unrecognized image file format\n");
				System.out.println("-h  			Print this message.\n");
			    return ;
			}
			
			if (cmd.hasOption("i")) {
				//log.log(Level.INFO, "Using cli argument -i=" + cmd.getOptionValue("i"));
			    INPUT_FILE = cmd.getOptionValue("i");
				File f = new File(INPUT_FILE);
				if (!f.isFile() || !f.canRead() || f.isDirectory()) {
					//log.log(Level.SEVERE, "Unable to load input file/folder");
					throw new IllegalArgumentException("ERROR: Unable to load input file/folder");
				}
				
			} else {
				//log.log(Level.SEVERE, "Missing -i option");
			    throw new IllegalArgumentException("Error: Missing -i option");
			}
			
			if (cmd.hasOption("o")) {
				//log.log(Level.INFO, "Using cli argument -o=" + cmd.getOptionValue("o"));
				OUTPUT_FLD = cmd.getOptionValue("o");
				File f = new File(OUTPUT_FLD);
				if (!f.canRead() || !f.isDirectory()) {
					//f.mkdirs();
					log.log(Level.INFO, "Folder(s) " + f.getAbsolutePath() + " created");
					
				}
			} else {
				//log.log(Level.SEVERE, "Missing -o option");
			    throw new IllegalArgumentException("Error: Missing -o option");
			}
			
			if(cmd.hasOption("content")){
				
				Content=true;
				
			}
			
			if(cmd.hasOption("topages")){
				
				ToPages=true;
				
			}
			
			
		}
		
		catch(MissingArgumentException e){
			System.out.println(	e.getLocalizedMessage());
			System.exit(1);
			}
		
		catch (ParseException e) {
			log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			//help();
			System.exit(1);
		}

		
		catch (IllegalArgumentException e) {
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		}
		
		
		OUTPUT_FILE = FilenameUtils.getBaseName(INPUT_FILE) +".html";
		tempFolderName = FilenameUtils.getBaseName(INPUT_FILE) + "-sources";
		
		unZipIt(INPUT_FILE, OUTPUT_FLD + File.separator + tempFolderName);
		
		File picturesDir = new File(OUTPUT_FLD + File.separator + tempFolderName + File.separator + "word" + File.separator + "media");
		if (picturesDir.exists()) {
			FileUtils.copyDirectory(picturesDir, new File(OUTPUT_FLD + File.separator + "img"));
		}

		ArrayList<String> css2include = new ArrayList<String>();
		css2include.add("local.css");
		css2include.add("theme.css");
		
		for (String file : css2include) {
			InputStream is = DOCX2ADF.class.getClassLoader().getResourceAsStream("css/" + file);
			FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + File.separator + "css" + File.separator + file));	
		}
		
		ArrayList<String> js2include = new ArrayList<String>();
		js2include.add("local.js");
		js2include.add("pdx-behavior.js");
		js2include.add("fixattr.js");
		for (String file : js2include) {
			InputStream is = DOCX2ADF.class.getClassLoader().getResourceAsStream("js/" + file);
			FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + File.separator + "js" + File.separator + file));	
		}
		/*
		ArrayList<String> fonts2include = new ArrayList<String>();
		fonts2include.add("cmunbi.otf");
		fonts2include.add("cmunbxo.otf");
		fonts2include.add("cmunbx.otf");
		fonts2include.add("cmunrm.otf");
		fonts2include.add("cmunsi.otf");
		fonts2include.add("cmunss.otf");
		fonts2include.add("cmunsx.otf");
		fonts2include.add("cmunti.otf");
		fonts2include.add("cmuntt.otf");
		for (String file : fonts2include) {
			InputStream is = DOCX2ADF.class.getClassLoader().getResourceAsStream("fonts/" + file);
			FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + File.separator + "fonts/" + File.separator + file));	
		}
		*/
		try {

            TransformerFactory tFactory = TransformerFactoryImpl.newInstance("net.sf.saxon.TransformerFactoryImpl", null);

            StreamSource xslStream = new javax.xml.transform.stream.StreamSource(DOCX2ADF.class.getClassLoader().getResourceAsStream(XSLT_FILE));
            Transformer transformer =
                    tFactory.newTransformer(xslStream);

            //transformer.setParameter("dir", OUTPUT_FLD + File.separator + tempFolderName + File.separator);
          transformer.setParameter("dir", OUTPUT_FLD + "/" + tempFolderName + "/" +"word"+ "/");
            transformer.setParameter("basecss", "css/");
            transformer.setParameter("basejs", "js/");
            
         //   transformer.setParameter("baserng", );
           transformer.setParameter("baseimg", "img/");
           
           if (Content){
        	   
        	   transformer.setParameter("content", true);
        	   
           }
           
           if(ToPages){
        	   
        	   transformer.setParameter("topages", true);
           }
            
            transformer.transform(
                    new javax.xml.transform.stream.StreamSource(OUTPUT_FLD + File.separator + tempFolderName + File.separator + "word" + File.separator + XML_FILE),
                    new javax.xml.transform.stream.StreamResult( new FileOutputStream(OUTPUT_FLD + File.separator + OUTPUT_FILE))
            );
		} catch (Exception e) {
            e.printStackTrace( );
		}
		
		File tempDir = new File(OUTPUT_FLD + File.separator + tempFolderName);
		if (tempDir.exists())
			FileUtils.deleteDirectory(tempDir);
		
		System.out.println("File " + INPUT_FILE + " successefully converted into ADF.");

	}

	
    
    /**
     * Extracts content.xml and pictures from an odt file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    static void unZipIt(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];
       	
        try{
       		
	       	//create output directory is not exists
	       	File folder = new File(outputFolder);
	       	if(!folder.exists()) {
	       		folder.mkdir();
	       	}
	       		
	       	//get the zip file content
	       	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
	       	//get the zipped file list entry
	       	ZipEntry ze = zis.getNextEntry();
	       		
	       	while(ze!=null){
	       			
	       	   String fileName = ze.getName();
	           File newFile = new File(outputFolder + File.separator + fileName);
	                   
	           //System.out.println("file unzip : "+ newFile.getAbsoluteFile());
	                   
	           //create all non exists folders
	           //else you will hit FileNotFoundException for compressed folder
	           new File(newFile.getParent()).mkdirs();
	                 
	           FileOutputStream fos = new FileOutputStream(newFile);             
	
	           int len;
	           while ((len = zis.read(buffer)) > 0) {
	        	   fos.write(buffer, 0, len);
	           }
	           		
	           fos.close();   
	           ze = zis.getNextEntry();
	       	}
	       	
	        zis.closeEntry();
	       	zis.close();
	       		
	       	//System.out.println("Done");
       		
        }catch(IOException ex) {
        	ex.printStackTrace();
        }
    }

}
