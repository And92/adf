$(document).ready(main);
function main() {
	var hash = location.search
	if (hash.indexOf('menu') == -1) {
		$('#menu').hide()
	} else {
		$('#menu').show()				
	}
	if (hash.indexOf('noauto') == -1) {
		$('*').pdx('setup') ; 
	}
	callbacksSetup();
}

function callbacksSetup() {
	$('#setupButton').on('click', function() {
		callPdx('setup');
	})
	$('#destroyButton').on('click', function() {
		callPdx('destroy');
	})
	$('#alphaButton').on('click', function() {
		reformat('alpha')
	})
	$('#apaButton').on('click', function() {
		reformat('apa')
	})
	$('#simpleButton').on('click', function() {
		startUpdate('#simple','latest')
	})
	$('#middleButton').on('click', function() {
		startUpdate('#middle','1')
	})
	$('#sructuredButton').on('click', function() {
		startUpdate('#structured','latest')
	}) 
	$('#menu h1 .fa').click() ;
}

function startUpdate(name,version) {
	var filter = '[typeof="Template"][resource="$name"]'.tpl({name:name})
	$(filter).pdx('update',{version:version})
}
function reformat(formatter) {
	$('[role~="pdx-formatter"]').pdx('update',{formatter:formatter})
}
function callPdx(command) {
	$('*').pdx(command) ; 
}

formatters.alpha = function(e, n) {
		var authors = [].concat(e.author || [],e.editor || [])
		if (!authors.length) {
			var authPart = "???"
		} else if (authors.length == 1) {
			var authPart = authors[0].substr(0,3)
		} else {
			var authPart = ""
			for (var i=0; i<3; i++) {
				authPart += authors[i].substr(0,1).toUpperCase()
			}
		}
		if (!e.date.length) {
			var datePart = "??"
		} else {
			var datePart = e.date[e.date.length-1].substr(-2)
		}
		return "["+authPart+datePart+"]"
	}
formatters.apa = function(e, n) {
		var authors = [].concat(e.author || [],e.editor || [])
		if (!authors.length) {
			var authPart = "???"
		} else if (authors.length == 1) {
			var authPart = authors[0]
		} else {
			var authPart = authors[0] + " et al."
		}
		if (!e.date.length) {
			var datePart = "??"
		} else {
			var datePart = e.date[e.date.length-1].substr(-4)
		}
		return "["+authPart+", "+datePart+"]"	
	}
