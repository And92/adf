<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:f="http://illbe.xyz/XSLT/function"
        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
        xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
        xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
        xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
        xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
        xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"
        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes"
        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
        xmlns:v="urn:schemas-microsoft-com:vml"
        exclude-result-prefixes="xs xd f xlink svg dc fo meta w r ">

   

    <xsl:output
            encoding="UTF-8"
            method="xml"
            indent="yes"
             />


	<xsl:param name="content" as="xs:boolean" select="false()" />
	
	<xsl:param name="topages" as="xs:boolean" select="false()" />
	
    <!--
        This parameters refers to the base path that all the URL of the CSS files
        of the final RASH document should have.
    -->
    <xsl:param name="basecss" select="'./'" />

    <!--
        This parameters refers to the base path that all the URL of the Javascript files
        of the final RASH document should have.
    -->
    <xsl:param name="basejs" select="'./'" />

    <!--
        This parameters refers to the base path that all the URL of the RelaxNG files
        of the final RASH document should have.
    -->
    <xsl:param name="baserng" select="'./'" />

    <!--
        This parameters refers to the base path that all the URL of the image files
        of the final RASH document should have.
    -->
    <xsl:param name="baseimg" select="'./'" />

    <!--
        This parameters refers to the directory that contains the actual XML content
        of the ODT document to transform.
    -->
    <xsl:param name="dir" select="'./'" />
    <!--<xsl:param name="dir" select="'../testbed/docx/testbed-8/word/'" />-->
    
    <!-- 
        This parameter refers to the action of keeping the bibliographic reference
        order as specified in the original document.
    -->
    <xsl:param name="keep-ref-order" select="false()" />

    <!-- This variable is used to remove separators in captions -->
    <xsl:variable name="subcap" select="'^[!,\.:;\?\|\-\s]+'" as="xs:string" />
    <!--
        These variables are used for identifying the text of the headings referring to the
        sections abstract, acknowledgements and bibliography
    -->
    <xsl:variable name="abstract" select="('abstract', 'summary')" as="xs:string+" />
    <xsl:variable name="acknowledgements" select="('acknowledgements', 'acknowledgement')" as="xs:string+" />
    <xsl:variable name="bibliography" select="('bibliography', 'references', 'reference')" as="xs:string+" />

    <!-- Link references document -->
    <xsl:variable name="links" select="doc(concat($dir, '/_rels/document.xml.rels'))" as="item()?" />

    <!-- Link styles document -->
    <xsl:variable name="styles" select="doc(concat($dir, '/styles.xml'))" as="item()?" />

    <!-- Link numbering document -->
    <xsl:variable name="numbering" select="doc(concat($dir, '/numbering.xml'))" as="item()?" />


    <xsl:variable name="footnotesExists" select="doc-available(concat($dir, '/footnotes.xml'))" as="xs:boolean" />
    <xsl:variable name="footnotes" select="doc(concat($dir, '/footnotes.xml'))" as="item()?" />

    <xd:doc scope="/">
        <xd:desc>
            <xd:p>This template is in charge of starting the transformation.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:apply-templates>
            <xsl:with-param name="isInsideBlock" select="false()" tunnel="yes" as="xs:boolean" />
            <xsl:with-param name="caption" select="false()" tunnel="yes" as="xs:boolean" />
        </xsl:apply-templates>
    </xsl:template>

    <xd:doc scope="w:body">
        <xd:desc>
            <xd:p>This template is in charge of creating the whole structure of the document in RASH.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:body">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <html xmlns="http://www.w3.org/1999/xhtml"	vocab="http://www.alstom.com/sse/"		
				  prefix="doc:http://www.alstom.com/sse/	tpl:http://www.alstom.com/sse/templates/">
            <head>
                <!-- Visualisation requirements (mandatory for optimal reading) -->
                <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
               
                
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js"> <xsl:text> </xsl:text> </script>
                <script src="{$basejs}pdx-behavior.js"><xsl:text> </xsl:text></script>
                <script src="{$basejs}local.js"><xsl:text> </xsl:text></script>
                <script src="{$basejs}fixattr.js"><xsl:text> </xsl:text></script>
                <link id="bootstrap" rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all"/>
               
                <link id="fontawesome" rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all"/>
                <link rel="stylesheet" href="{$basecss}local.css"></link>
                <link rel="stylesheet" href="{$basecss}theme.css"></link>

            </head>
            
            <body>
            
            <xsl:if test="not($content) and not($topages)">
            
            <section	id="frontmatter"	role="doc-frontmatter pdx-non-hcountable"	class="doc-ro">	
																	<!--	Content	of	the	front	matter	-->	
					
					<xsl:apply-templates select="(w:p[f:isHeading(.)][f:containsText(.)][1]/preceding-sibling::w:tbl)"/>
					
					<section id="toc" role="pdx-non-hcountable">
						<h1>Contents</h1>
						<section	role="pdx-htabler"><i>No	table	of	content</i></section>												
					</section>
				
					<section	id="tof"	role="pdx-non-hcountable">	
	 					<h1><span	role="pdx-hcounter"></span>Figure</h1>	
	 					<section	role="pdx-tabler"		data-pdx-family="images"><i>Indice	delle	figure	assente</i></section>	
					</section>
					
					<section	id="tot"	role="pdx-non-hcountable">	
	 					<h1><span	role="pdx-hcounter"></span>Tabelle</h1>	
	 					<section	role="pdx-tabler"		data-pdx-family="tables"><i>Indice	delle	tabelle	assente	</i></section>	
					</section>
					
				</section>
				
				 </xsl:if>
						
						<section	id="content"	role="doc-content pdx-non-hcountable">
							<xsl:apply-templates select="w:p[f:isHeading(.)][f:containsText(.)][1]" />
						</section>

					<!--	Footnotes	-->	

                 <xsl:call-template name="add.footnotes" /> 
            </body>
        </html>
    </xsl:template>

    <xd:doc scope="w:p">
        <xd:desc>
            <xd:p>This template is in charge of handling common paragraphs.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:p">
			<xsl:call-template name="add.p">
            <xsl:with-param name="isInsideList" select="false()" />
            <xsl:with-param name="isInsideNote" select="false()" />
        	</xsl:call-template> 
    </xsl:template>


    <xd:doc scope="w:r">
        <xd:desc>
            <xd:p>This template handles all the inline textual elements that appear in the context of a paragraph, excluding links.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:r">
        <xsl:param name="isInsideBlock" as="xs:boolean" tunnel="yes" />
        <xsl:variable name="notvisual" select="exists(.//v:shape/v:imagedata/@r:id)" as="xs:boolean" />
        <xsl:variable name="isBold" select="exists(w:rPr/w:b[not(@w:val) or @w:val!=0])" as="xs:boolean" />
        <xsl:variable name="isUnderline" select="exists(w:rPr/w:u[not(@w:val) or @w:val!='none'])" as="xs:boolean" />
        <xsl:variable name="isItalic" select="exists(w:rPr/w:i)" as="xs:boolean" />
        <xsl:variable name="isSuperscript" select="w:rPr/w:vertAlign/@w:val = 'superscript'" as="xs:boolean" />
        <xsl:variable name="isSubscript" select="w:rPr/w:vertAlign/@w:val = 'subscript'" as="xs:boolean" />
        <xsl:variable name="isNote" select="f:isNote(.)" as="xs:boolean" />
        <xsl:variable name="isImage" select="f:containsAnImage(.)" as="xs:boolean" />
        <xsl:variable name="isRef" as="xs:boolean" select="f:isInsideRef(.)" />
      <!-- da togliere -->  <xsl:variable name="lastFormula" as="element()?" select="preceding-sibling::m:oMath[last()]" /> 
    <!-- da togliere -->   <xsl:variable name="nextFormula" as="element()?" select="following-sibling::m:oMath[1]" /> 
		
		
		<xsl:if test="$notvisual" >
			<xsl:variable name="dim" as="xs:string?" select="string-join(for $i in tokenize(.//v:shape/@style,';') return (if (contains($i,'width') or contains($i,'height')) then $i else ''),';')"/>
			<xsl:variable name="iden" as="xs:string?" select=".//v:shape/v:imagedata/@r:id"/>
		
			<xsl:call-template name="add.image.ob" >
		
				<xsl:with-param name="iden" select="$iden"/>
				<xsl:with-param name="dim" select="$dim"/>
		
			</xsl:call-template>
		
		</xsl:if>
		
		<xsl:if test="not($notvisual)">
	
        <!-- If the immediately previous element doesn't have the same style then proceed -->
        <xsl:variable name="previousElementSameStyle" as="xs:boolean">
            <xsl:variable name="prev" select="f:getPrecedingInlines(.)[last()]" as="item()?" /> <!-- sel elemento imm prima -->
            <xsl:variable name="prevIsBold" select="exists($prev/w:rPr/w:b[not(@w:val) or @w:val!=0])" as="xs:boolean" />
            <xsl:variable name="prevIsUnderline" select="exists($prev/w:rPr/w:u[not(@w:val) or @w:val!='none'])" as="xs:boolean" /> 
            <xsl:variable name="prevIsItalic" select="exists($prev/w:rPr/w:i)" as="xs:boolean" />
            <xsl:variable name="prevIsSuperscript"
                          select="$prev/w:rPr/w:vertAlign/@w:val = 'superscript'" as="xs:boolean" />
            <xsl:variable name="prevIsSubscript"
                          select="$prev/w:rPr/w:vertAlign/@w:val = 'subscript'" as="xs:boolean" />
            <xsl:variable name="prevIsNote" select="f:isNote($prev)" as="xs:boolean" />
            <xsl:variable name="prevIsImage" select="f:containsAnImage($prev)" as="xs:boolean" />
            <xsl:variable name="prevIsRef" select="f:isInsideRef($prev)" as="xs:boolean" />
            <xsl:value-of select="
                    $prev[self::w:r]
                    and $isBold = $prevIsBold and $isUnderline = $prevIsUnderline and $isItalic = $prevIsItalic 
                    and  $isSuperscript = $prevIsSuperscript 
                    and $isSubscript = $prevIsSubscript
                    and $isNote = $prevIsNote and $isImage = $prevIsImage and $isRef = $prevIsRef
                    and not(preceding-sibling::*[1] is $lastFormula)
                "
            />
        </xsl:variable>

        <xsl:if test="not($previousElementSameStyle)">
            <!-- The following elements that doesn't have the same kind of text -->
            <xsl:variable name="follTNotEqual" as="element()*">
                <xsl:for-each select="f:getFollowingInlines(.)">
                    <xsl:variable name="follIsBold" select="exists(w:rPr/w:b[not(@w:val) or @w:val!=0])" as="xs:boolean" />
                    <xsl:variable name="follIsItalic" select="exists(w:rPr/w:i)" as="xs:boolean" />
                    <xsl:variable name="follIsUnderline" select="exists(w:rPr/w:u[not(@w:val) or @w:val!='none'])" as="xs:boolean" />
                    <xsl:variable name="follIsSuperscript" select="w:rPr/w:vertAlign/@w:val = 'superscript'" as="xs:boolean" />
                    <xsl:variable name="follIsSubscript" select="w:rPr/w:vertAlign/@w:val = 'subscript'" as="xs:boolean" />
                    <xsl:variable name="follIsNote" select="f:isNote(.)" as="xs:boolean" />
                    <xsl:variable name="follIsImage" select="f:containsAnImage(.)" as="xs:boolean" />
                    <xsl:variable name="follInsideRef" select="f:isInsideRef(.)" as="xs:boolean" />
                    <xsl:if test="
                        not(self::w:r)
                        or not(
                            $isBold = $follIsBold and $isUnderline = $follIsUnderline and $isItalic = $follIsItalic 
                            and $isSuperscript = $follIsSuperscript and $isSubscript = $follIsSubscript
                            and $isNote = $follIsNote and $isImage = $follIsImage and $isRef = $follInsideRef
                        )"
                    >
                        <xsl:sequence select="." />
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>

            <!-- All the following elements that have the same kind of text -->
            <xsl:variable name="follTEqual" as="element()*">
                <xsl:for-each select="f:getFollowingInlines(.)">
                    <xsl:variable name="follIsBold" select="exists(w:rPr/w:b[not(@w:val) or @w:val!=0])" as="xs:boolean" />
                    <xsl:variable name="follIsItalic" select="exists(w:rPr/w:i)" as="xs:boolean" />
                    <xsl:variable name="follIsUnderline" select="exists(w:rPr/w:u[not(@w:val) or @w:val!='none'])" as="xs:boolean" />
                    <xsl:variable name="follIsSuperscript" select="w:rPr/w:vertAlign/@w:val = 'superscript'" as="xs:boolean" />
                    <xsl:variable name="follIsSubscript" select="w:rPr/w:vertAlign/@w:val = 'subscript'" as="xs:boolean" />
                    <xsl:variable name="follIsNote" select="f:isNote(.)" as="xs:boolean" />
                    <xsl:variable name="follIsImage" select="f:containsAnImage(.)" as="xs:boolean" />
                    <xsl:variable name="follInsideRef" select="f:isInsideRef(.)" as="xs:boolean" />
                    <xsl:if test="
                        self::w:r
                        and $isBold = $follIsBold  and $isUnderline = $follIsUnderline
                        and $isItalic = $follIsItalic
                        and $isSuperscript = $follIsSuperscript and $isSubscript = $follIsSubscript
                        and $isNote = $follIsNote and $isImage = $follIsImage and $isRef = $follInsideRef"
                    >
                        <xsl:sequence select="." />
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$isRef">
                    <xsl:variable name="nextRs"
                                  as="element()*"
                                  select="following-sibling::w:r"
                    />
                    <xsl:variable name="closingRef"
                                  as="element()*"
                                  select="following-sibling::w:r[w:fldChar/@w:fldCharType = 'end'][1]"
                    />
                    <xsl:variable name="refRs"
                                  as="element()*"
                                  select="$nextRs except $closingRef/following::w:r"
                    />
                    <xsl:variable name="refElements"
                                  as="element()*"
                                  select="., $refRs, $closingRef"
                    />
                    
                    <xsl:variable name="titletab" as="xs:string?" select="lower-case(normalize-space(f:getStringFromTextNodes(ancestor::w:tbl/w:tr[1]//w:t)))"  />
                    <xsl:variable name="rev" as="xs:boolean" select="contains($titletab,'revisioni') or contains($titletab,'revisions') "/>
                    
                    <xsl:choose>
                        <xsl:when test="some $r in $refElements satisfies f:getStyleName($r) = 'footnote reference'">
                            <a href="#ftn{$refElements//w:t}"><xsl:text> </xsl:text></a>
                        </xsl:when>
                        <xsl:when test="$rev">
                            <xsl:value-of select="f:getStringFromTextNodes($refElements//w:t)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="add.ref">
                                <xsl:with-param name="refElements" select="$refElements" />
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="add.inline">
                     <xsl:with-param name="select"
                                        tunnel="yes"
                                        select="(.//w:t, ($follTEqual except ($follTNotEqual, f:getFollowingInlines($follTNotEqual), f:getFollowingInlines($nextFormula)))/w:t)" /> 
                                        
                               <xsl:with-param name="s1"
                                        tunnel="yes"
                                        select="(., ($follTEqual except ($follTNotEqual, f:getFollowingInlines($follTNotEqual), f:getFollowingInlines($nextFormula))))" />         
                        <xsl:with-param name="super" as="xs:boolean" tunnel="yes" select="$isSuperscript and not($isInsideBlock)" />
                        <xsl:with-param name="underline" as="xs:boolean" tunnel="yes" select="$isUnderline and not($isInsideBlock)" />
                        <xsl:with-param name="sub" as="xs:boolean" tunnel="yes" select="$isSubscript and not($isInsideBlock)" />
                        <xsl:with-param name="bold" as="xs:boolean" tunnel="yes" select="$isBold and not($isInsideBlock)" />
                        <xsl:with-param name="italic" as="xs:boolean" tunnel="yes" select="$isItalic and not($isInsideBlock)" />
                        <xsl:with-param name="note" as="xs:boolean" tunnel="yes" select="$isNote and not($isInsideBlock)" />
                        <xsl:with-param name="image" as="xs:boolean" tunnel="yes" select="$isImage and not($isInsideBlock)" />
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
       </xsl:if>
    </xsl:template>
    
    
    <!-- Template alternativo per gestire immagini -->
    
    <xsl:template name="add.image.ob" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
    
    <xsl:param name="iden" as="xs:string"/>
    <xsl:param name="dim" as="xs:string?"/>
    
    <xsl:variable name="ext" as="xs:string?" select="f:getImageNameById($iden)" />
    
    <xsl:choose>
    
    	<xsl:when test="ends-with($ext,'png') or ends-with($ext,'jpeg') ">
    	
    	<!--	<figure data-pdx-family="images"> 
    		
        		<xsl:attribute name="id">
        	
        			<xsl:value-of select="concat('figure',generate-id())"/>
        	
        		</xsl:attribute> -->
    	
    		<xsl:variable name="alt" as="xs:string" select="f:getCaptionText(.)" />
       	<img style="{$dim}" src="{$baseimg}{substring-after(f:getImageNameById($iden), 'media/')}"
             alt="{if (string-length($alt) > 0) then $alt else 'No alternate description has been provided.'}" /> 
             
       <!--      <xsl:call-template name="add.captionimg" />
    	
    		</figure> -->
    	</xsl:when>
    	
    	<xsl:when test="$topages" >
    	
    	<span> <xsl:text> </xsl:text>  </span>
    	
    	</xsl:when>
    
    	<xsl:otherwise>
    	
  <!--  	<p>
    	<xsl:attribute name="style" select="concat('overflow: auto;','border: 1px solid black;',$dim,';')"/>

    	Immagine non visualizzabile
    	</p> -->
    	
    	
 <!--   	<figure data-pdx-family="images">
    		
        		<xsl:attribute name="id">
        	
        			<xsl:value-of select="concat('figure',generate-id())"/>
        	
        		</xsl:attribute> -->
    	
    		<xsl:variable name="alt" as="xs:string" select="f:getCaptionText(.)" />
        	<img style="{$dim}" src="{$baseimg}{substring-after(f:getImageNameById($iden), 'media/')}"
             alt="Immagine non visualizzabile" />
             
     <!--        <xsl:call-template name="add.captionimg" />
    	
    		</figure> -->
    	
    	</xsl:otherwise>
    
    
    </xsl:choose>
    
    
    </xsl:template>
    
    

    <xd:doc scope="w:hyperlink">
        <xd:desc>
            <xd:p>This template handles all the inline external (i.e., to an external website) links that appear in the context of a paragraph, excluding links.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:hyperlink" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
        <xsl:variable name="id" select="@r:id" as="xs:string" />
        <a href="{$links//Relationship[@Id = $id]/@Target}">
            <xsl:apply-templates />
        </a>
    </xsl:template>

    <xd:doc scope="w:pPr">
        <xd:desc>
            <xd:p>This template is used for avoiding to process certain elements.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:hyperlink[not(exists(@r:id))] | w:pPr | w:rPr | w:sectPr | w:p[f:getStyleName(.) = 'Title'] | w:p[f:getStyleName(.) = 'Subtitle']" />

    <xd:doc scope="add.ref">
        <xd:desc>
            <xd:p>This template creates all the references to dereferanceable objects in the document content (i.e., sections, figures with caption, formulas with caption, and tables).</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.ref">
        <xsl:param name="refElements" as="element()*" />
        <xsl:variable name="refLink"
                      as="xs:string"
                      select="
                      normalize-space(
                        substring-before(
                          substring-after($refElements[w:instrText][1]/w:instrText/text(), 'REF'),
                          '\'
                        )
                      )" />
         <xsl:variable name="refText" as="xs:string" select="string-join($refElements//w:t/text(),'')" />             
                      
        <a href="#{$refLink}"><xsl:value-of select="$refText"/></a>
      <!--  <a href="#{$refLink}"><xsl:text> </xsl:text></a>-->
    </xsl:template>

    <xd:doc scope="w:p[m:oMathPara]">
        <xd:desc>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:p[m:oMathPara]">
        <figure>
        
        <!--    <xsl:call-template name="set.captioned.object.id" /> -->
            <p>
                <xsl:apply-templates />
            </p>
        </figure>
    </xsl:template>

    <xd:doc scope="m:oMath">
        <xd:desc>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="m:oMath">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:apply-templates />
        </math>
    </xsl:template>
    
    
    
 
    

    <xd:doc scope="w:tbl">
        <xd:desc>
            <xd:p>This template creates new table boxes.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:tbl">
    <xsl:variable name="table" as="element()">
    
    <xsl:choose>
    
    	<xsl:when test="some $i in ./preceding-sibling::w:p satisfies starts-with(f:getStyleName($i),'toc')">
    
        <figure role="pdx-countable" data-pdx-family="tables" >
           <xsl:call-template name="set.captioned.object.id" /> 
            <table >
              <xsl:attribute name="class" >
    		 	<xsl:value-of select="('table table-bordered table-responsive')" />
    		 </xsl:attribute>
    		 
                <xsl:apply-templates />
            </table>
            <xsl:call-template name="add.caption" />
        </figure>
        
        </xsl:when>
        
        <xsl:otherwise>
        
        <table>
        	<xsl:attribute name="class" >
    			<xsl:value-of select="('table table-bordered table-responsive')" />
    		</xsl:attribute>
         	<xsl:apply-templates />
        </table>
        
        </xsl:otherwise>
        
        </xsl:choose>
        
        </xsl:variable>
        <xsl:choose>
      <!--  <xsl:when test="contains(f:TitleEl(.),'riferimento')">
        <xsl:call-template name="add.table.rif">
        	<xsl:with-param name="tab" select="$table" as="element()"/>
        </xsl:call-template>
        </xsl:when>
        -->
        
        <xsl:when test="contains(f:TitleEl(.),'acronimi')">
        <xsl:call-template name="add.table.acr">
        	<xsl:with-param name="tab" select="$table" as="element()"/>
        </xsl:call-template>
        </xsl:when>
        
        <xsl:otherwise>
        
        <xsl:copy-of select="$table" />
        
        </xsl:otherwise>
        
        </xsl:choose>
    </xsl:template>
    
    
    <!-- Template per creare tabella revisioni -->
    
    
   <!-- <xsl:template name="add.table.rev" xpath-default-namespace="http://www.w3.org/1999/xhtml">
    
    <xsl:param name="tab" as="element()" />
    
    <section id="tor" role="pdx-non-hcountable"> 
     
    <figure id="tor_table" >
    
     <table>
     <xsl:attribute name="class" >
    		<xsl:value-of select="('table table-bordered table-responsive')" />
    	</xsl:attribute>
    	
    	<tr>
    	
    	<xsl:for-each select="$tab//tr[position()=1 or position() = 2 ]">
    	
    		<xsl:choose>
    		
    			<xsl:when test="./child::th">
    			
    			<th><xsl:value-of select="f:getStringFromTextNodes(.)" /></th>
    			
    			</xsl:when>
    		
    		
    		
    		</xsl:choose>
    		
    	
    	</xsl:for-each>
    	
    	
    	</tr>
    	
    	<xsl:for-each select="$tab//tr[position()>1]">
    	
    	<xsl:call-template name="bod.rev">
    	
    	
    	<xsl:with-param name="curel" select="." as="element()"/>
    	
    	</xsl:call-template>
   	
    	
    	</xsl:for-each>
    
    </table>

    
    
    
    </figure>
    
    </section>
    
    </xsl:template> -->
    
    <!-- Template per creare corpo tabella revisioni -->
    
    
    
    
   
    <!-- Template per creare tabella acronimi -->
    
    <xsl:template name="add.table.acr" xpath-default-namespace="http://www.w3.org/1999/xhtml">
    
    <xsl:param name="tab" as="element()" />
     
    <figure role="pdx-countable" data-pdx-family="tables" >
    
    <xsl:if test="$tab/@ab-fig">
    
    <xsl:attribute name="ab-fig">
    
    	<xsl:value-of select="$tab/@ab-fig" />
    
    </xsl:attribute>
    
    </xsl:if>
    
    
     <table>
     <xsl:attribute name="class" >
    		<xsl:value-of select="('table table-bordered table-responsive')" />
    	</xsl:attribute>
    	
    	<tr>
    	
    	<xsl:for-each select="$tab//tr[position()=1]/th">
    	
    		
    		<xsl:copy-of select="."/>
    		
    	
    	</xsl:for-each>
    	
    	
    	</tr>
    	
    	<xsl:for-each select="$tab//tr[position()>1]">
    	
    	<xsl:call-template name="bod.acr">
    	
    	
    	<xsl:with-param name="curel" select="." as="element()"/>
    	
    	</xsl:call-template>
   	
    	
    	</xsl:for-each>
    
    </table>
    <figcaption>
    
    Tabella<span role="pdx-incrementer" data-pdx-family="tables"><xsl:text> </xsl:text> </span>-Acronimi
    
    </figcaption>
    
    
    
    </figure>
    
    
    
    </xsl:template>
    
    
    
    <!-- Template per creare corpo tabella acronimi -->
    
    <xsl:template name="bod.acr" xpath-default-namespace="http://www.w3.org/1999/xhtml">
    
    <xsl:param name="curel" as="element()"/>
    
    <tr typeof="Acronym" resource="{concat('#acronym_',f:getStringFromTextNodes($curel/td[1]))}">
    
    <xsl:for-each select="$curel/td">
    <td>
    	<p>
    	
    		<xsl:choose>
    		
    			<xsl:when test="position()=1">
    			
    				<xsl:attribute name="property" select="('abbreviation')"/>
    				
    				
    				<!-- dsd -->



    			
    				<xsl:if test="exists(./p/@id)" >
    			
    				<xsl:attribute name="ab-acr">
    					
    					<xsl:value-of select="./p/@id"/>
    					
    				</xsl:attribute>
    				
    				</xsl:if>
    				
    				
    				<!-- ccc -->
    				
    				
    			
    			</xsl:when>
    			
    			<xsl:when test="position()=2">
    			
    				<xsl:attribute name="property" select="('definition')"/>
    			
    			</xsl:when>
    		
    			
    		
    		</xsl:choose>
    		
    		<xsl:value-of select="f:getStringFromTextNodes(./p)" />
    		
    
    	</p>
    
    </td>
    
    
    </xsl:for-each>
    
    </tr>
    
    </xsl:template>
    
    <!-- Template per creare tabella riferimenti -->
    
    <xsl:template name="add.table.rif" xpath-default-namespace="http://www.w3.org/1999/xhtml">
    
    <xsl:param name="tab" as="element()" />
    <figure role="pdx-countable" data-pdx-family="tables" >
    
    <xsl:if test="$tab/@ab-fig">
    
    <xsl:attribute name="ab-fig">
    
    	<xsl:value-of select="$tab/@ab-fig" />
    
    </xsl:attribute>
    
    </xsl:if>
    
     <table>
     	<xsl:attribute name="class" >
    		<xsl:value-of select="('table table-bordered table-responsive')" />
    	</xsl:attribute>
    	<tr>
    	
    	<xsl:for-each select="$tab//tr[position()=1]/th">
    	
    		
    		<xsl:copy-of select="."/>
    		
    	
    	</xsl:for-each>
    	
    	
    	</tr>
    	
    	
    	 
    	
    	<xsl:for-each select="$tab//tr[position()>1]">
    	
    	<xsl:call-template name="bod">
    	
    	<xsl:with-param name="level" select="position()" as="xs:integer"/>
    	
    	<xsl:with-param name="curel" select="." as="element()"/>
    	
    	</xsl:call-template>
   	
    	
    	</xsl:for-each>
    	 
     </table>
    
    <figcaption>
    
    Tabella<span role="pdx-incrementer" data-pdx-family="tables"><xsl:text> </xsl:text> </span>-Documenti di Riferimento
    
    </figcaption>
    
    </figure>
    
    
    </xsl:template>
   
    <!-- Template per creare corpo tabella riferimenti -->
   
    <xsl:template name="bod" xpath-default-namespace="http://www.w3.org/1999/xhtml">
    
    <xsl:param name="level" as="xs:integer"/>
    <xsl:param name="curel" as="element()"/>
    
    <tr typeof="BibliographicReference" resource="{concat('#document_R',$level)}" id="{concat('document_R',$level)}">
    
    <xsl:for-each select="$curel/td">
    
    <xsl:choose>
    
    	<xsl:when test="position() = 1">
    	
    		<td> 
    		
    			<p  property="shortref">
    			
    				<xsl:if test="exists(./p/@id)" >
    			
    				<xsl:attribute name="ab-ref">
    					
    					<xsl:value-of select="./p/@id"/>
    					
    				</xsl:attribute>
    				
    				<xsl:attribute name="id">
    					
    					<xsl:value-of select="./p/@id"/>
    					
    				</xsl:attribute>
    				
    				</xsl:if>
    		
    				<xsl:value-of select="concat('R[',$level,']')"/>
    		
    		 	</p> 
    		 
    		</td>
    	</xsl:when>
    	
    	<xsl:otherwise>
    	
    	<td>
    	<p>
    		<xsl:if test="position()=2">
    		
    			<xsl:attribute name="property" select="('title')"/>
    		
    		</xsl:if> 
    	<xsl:value-of select="f:getStringFromTextNodes(./p/child::text())"/> 
    	</p>
    	</td>
    	</xsl:otherwise>
    
    
    
    </xsl:choose>
    
    </xsl:for-each>
    	
    
    </tr>
    
    
    </xsl:template>
    
    
    
    

    <xd:doc scope="w:tr">
        <xd:desc>
            <xd:p>This template creates table rows.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:tr">
     
        <xsl:variable name="isHeading"
                      as="xs:boolean"
                      select="((./preceding-sibling::w:tblPr/w:tblLook/@w:firstRow = 1) or (./preceding-sibling::w:tblPr/w:tblLook/@w:firstRow = 0))
                        and (not(exists(./preceding-sibling::w:tr))) and not(f:containsAnImage(.))"
        /> <!-- f:containsAnImage e inserito per motivi di visualizzazione del cartiglio  -->
        <tr>
        
     
        	
        	
        	<xsl:apply-templates>
         	<xsl:with-param name="isHeading" as="xs:boolean" select="$isHeading" tunnel="yes" />
         </xsl:apply-templates>
        	
     
        
         </tr>
        
    </xsl:template>

    <xd:doc scope="w:tc">
        <xd:desc>
            <xd:p>This template creates new table heading cells.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:tc">
        <xsl:param name="isHeading" as="xs:boolean" tunnel="yes" />
        
        <xsl:choose>
            <xsl:when test="$isHeading">
                <th>
                <xsl:if test="exists(./w:tcPr/w:gridSpan/@w:val)">
                
                	<xsl:attribute name="colspan" >
                	
                		<xsl:value-of select="./w:tcPr/w:gridSpan/@w:val"/>
                	
                	</xsl:attribute>
                
                </xsl:if>
                    <xsl:apply-templates select="./w:p/child::*" />
                </th>
            </xsl:when>
            <xsl:otherwise>

            
            	<td>
            	<xsl:if test="exists(./w:tcPr/w:gridSpan/@w:val)">
                
                	<xsl:attribute name="colspan" >
                	
                		<xsl:value-of select="./w:tcPr/w:gridSpan/@w:val"/>
                	
                	</xsl:attribute>
                
                </xsl:if>
                    <xsl:apply-templates/>
                 
                </td>

                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="w:t">
        <xd:desc>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="w:t">
        <xsl:apply-templates />
    </xsl:template>

    <xd:doc scope="text()">
        <xd:desc>
            <xd:p>This template handles all text nodes and, in case there is an inline quotation, it adds the appropriate element to the RASH document.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="text()">
        <xsl:for-each select="f:sequenceOfTextNodes(.,())">
            <xsl:variable name="isQuote" select="position() mod 2 = 0" as="xs:boolean" />
            <xsl:choose>
                <xsl:when test="$isQuote">
                    <q><xsl:value-of select="normalize-space(.)" /></q>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="." />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xd:doc scope="add.p">
        <xd:desc>
            <xd:p>This template is in charge of handling common paragraphs.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.p">
        <xsl:param name="isInsideList" as="xs:boolean" />
        <xsl:param name="isInsideNote" as="xs:boolean" />
        
        
        <xsl:variable name="parent" select="parent::w:body" as="element()?" />
        <xsl:choose>
            <xsl:when test="f:pIsCaption(.)  or (f:pIsBetweenTwoListItems(.) and not($isInsideList) and not(f:isHeading(.))  ) or f:isLastListParagraph(.) ">
                <!--Ignore-->
            </xsl:when>
            <!-- Headings -->
            <xsl:when test="f:isHeading(.) ">
                <xsl:call-template name="create.section.with.heading" />
            </xsl:when>
            <!-- When a pure text paragraph is defined in the DOCX document without providing any particular structured
            organisation of it into a paper (such as defining headings), a section is created automatically in the final
            RASH document. -->
            <xsl:when test="$parent and f:getContentChildElements($parent)[1] is . "><!-- il blocco w:p deve essere il primo figlio del body -->
                <xsl:call-template name="create.untitled.section" />
            </xsl:when>

            <xsl:when test="contains(f:getStyleName(.), 'Quote')">
                <xsl:call-template name="add.quote" />
            </xsl:when>
            
            <xsl:when test="f:pIsListElement(.) and not($isInsideList) and ( not(exists(./parent::w:tc)) or count(./following-sibling::w:p) != 0 or count(./preceding-sibling::w:p) != 0  )">
                <xsl:if test="f:isFirstItemOfAList(.)">
                    <xsl:call-template name="add.list" />
                </xsl:if>
            </xsl:when>
            
      		<xsl:when test="f:containsAnImage(.) and not(f:containsText(.) ) and count(./descendant::w:drawing[.//pic:blipFill/a:blip/@r:embed]) = 1">
      		
      			<xsl:choose>
      			
      			<xsl:when test="some $i in ./preceding-sibling::w:p satisfies starts-with(f:getStyleName($i),'toc')" >
      			
      				<xsl:call-template name="add.image" />
      			
      			</xsl:when>
      			
      			<xsl:otherwise>
      		
                <xsl:call-template name="add.image.inline" />
                
               
               </xsl:otherwise>
                
               </xsl:choose>
                
            </xsl:when> 
       
          
            <!-- This is the basic case for the creation of paragraphs. -->
            <xsl:otherwise>
            <!-- vv-->
            <xsl:choose>
                <xsl:when test="f:containsText(.) or f:containsAnImage(.) ">

                     <p>

                     <!-- serve per settare l'attributo property dentro a p -->
                     
                     <xsl:if test="some $i in (./w:bookmarkStart) satisfies starts-with($i/@w:name,'_Ref') ">
                     	
                     <xsl:call-template name="set.bookmarked.object.id" /> 
                   
                     </xsl:if>
                        
                        <xsl:apply-templates />
                    </p>
                   
                </xsl:when>
                
                
                <xsl:when test ="exists(./w:r//v:shape/v:imagedata/@r:id) and count(.//w:r) = 1">
                
                	<xsl:choose>
      			
      					<xsl:when test="some $i in ./preceding-sibling::w:p satisfies starts-with(f:getStyleName($i),'toc')" >
      			
      						<figure data-pdx-family="images" role="pdx-countable">
      						 <xsl:call-template name="set.captioned.object.id" /> 
             					<p><xsl:apply-templates /> </p>
									<xsl:call-template name="add.captionimg" />
             				</figure>
      			
      					</xsl:when>
      			
      				<xsl:otherwise>
      		
                		<p><xsl:apply-templates /> </p>
                
               
               		</xsl:otherwise>
                
               </xsl:choose>
                
                </xsl:when>
                <!-- per tabella riferimenti -->
                <xsl:when test ="not(f:containsText(.)) and exists(./w:bookmarkStart) and count(./w:bookmarkStart) = 1">
                <xsl:if test="starts-with(./w:bookmarkStart/@w:name,'_Ref')">
                <p>
                <xsl:call-template name="set.bookmarked.object.id" />
                	<xsl:apply-templates />
                </p>	
                </xsl:if>
                </xsl:when>
                
                <xsl:otherwise/>
                
                </xsl:choose>
                
                <!-- vvv -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="add.quote">
        <xd:desc>
            <xd:p>This template is in charge of handling a cited paragraph.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.quote">
        <blockquote><p>
            <xsl:apply-templates />
        </p></blockquote>
    </xsl:template>

    <xd:doc scope="add.caption">
        <xd:desc>
            <xd:p>This named template is in charge of creating captions to figures or tables.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.caption">
    		<xsl:param name="parC" as ="xs:string?" />
        <xsl:variable name="caption" as="xs:string" select="f:getCaptionText(.)" />
        <figcaption>
            <xsl:choose>
                <xsl:when test="string-length($caption) > 0">
                   Tabella- <span	role="pdx-incrementer"	data-pdx-family="tables"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>
                   
                   <xsl:value-of select="$caption"/>
              
                    
                </xsl:when>
                
                <xsl:when test="$parC"> 
                
                 Tabella- <span	role="pdx-incrementer"	data-pdx-family="tables"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>
                   
                   <xsl:value-of select="$parC"/>
                
                </xsl:when> 
                <xsl:otherwise>
                    
                    Tabella- <span	role="pdx-incrementer"	data-pdx-family="tables"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>
                    
                </xsl:otherwise>
            </xsl:choose>
        </figcaption>
    </xsl:template>
    
    
    
      <xsl:template name="add.captionimg">
        <xsl:variable name="caption" as="xs:string" select="f:getCaptionText(.)" />
        <figcaption>
            <xsl:choose>
                <xsl:when test="string-length($caption) > 0">
                   Figura <span	role="pdx-incrementer"	data-pdx-family="images"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="$caption"/>
                </xsl:when>
                <xsl:otherwise>
                    Figura <span	role="pdx-incrementer"	data-pdx-family="images"><xsl:text> </xsl:text></span><xsl:text> </xsl:text> No caption has been provided.
                </xsl:otherwise>
            </xsl:choose>
        </figcaption>
    </xsl:template>
    
   
    <xd:doc scope="add.image">
        <xd:desc>
            <xd:p>This named template is in charge of adding a captioned image to the document.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.image" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
    
     <xsl:variable name="imageId"
                      as="xs:string"
                      select=".//pic:blipFill[not(ancestor::mc:Fallback)]/a:blip/@r:embed"
      />
      
      
      <xsl:choose>
      
      <xsl:when test="ends-with(f:getImageNameById($imageId),'png') or ends-with(f:getImageNameById($imageId),'jpeg') ">
    
    
    
        <figure data-pdx-family="images" role="pdx-countable">
        
           <xsl:call-template name="set.captioned.object.id" /> 
            <p>
                <xsl:call-template name="add.image.inline" />
            </p>
            <xsl:call-template name="add.captionimg" />
            
        </figure>
        
       </xsl:when>
       
       
       
        <xsl:when test=" $topages ">
    		<span> <xsl:text> </xsl:text> </span>
        </xsl:when>
        
        
        <xsl:otherwise>
        
        
        <figure data-pdx-family="images" role="pdx-countable">
        
          <xsl:call-template name="set.captioned.object.id" />
            <p>
                <xsl:call-template name="add.image.inline" />
            </p>
            <xsl:call-template name="add.captionimg" />
            
        </figure>
        
        
        </xsl:otherwise>
        
       </xsl:choose>
        
        
    </xsl:template>

    <xd:doc scope="add.image.inline">
        <xd:desc>
            <xd:p>This named template is in charge of adding an inline image to the document.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.image.inline" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
        <xsl:variable name="imageId"
                      as="xs:string"
                      select=".//pic:blipFill[not(ancestor::mc:Fallback)]/a:blip/@r:embed"
        />
        
        <xsl:variable name="alte" as="xs:double" select="(.//pic:spPr[not(ancestor::mc:Fallback)]/a:xfrm/a:ext/@cy) div 360000 " />
        <xsl:variable name="larg" as="xs:double" select="(.//pic:spPr[not(ancestor::mc:Fallback)]/a:xfrm/a:ext/@cx) div 360000 " />
        <xsl:variable name="s" as="xs:string" select="concat('height:',string($alte),'cm;','width:',string($larg),'cm;')" />
        <xsl:variable name="alt" as="xs:string" select="f:getCaptionText(.)" />
        
        <xsl:choose>
        
        <xsl:when test="ends-with(f:getImageNameById($imageId),'png') or ends-with(f:getImageNameById($imageId),'jpeg') ">
        
        <img style="{$s}" class="img-responsive" src="{$baseimg}{substring-after(f:getImageNameById($imageId), 'media/')}"
             alt="{if (string-length($alt) > 0) then $alt else 'No alternate description has been provided.'}" />
             
        </xsl:when>
        
        
        <xsl:when test="$topages">
        
        <span> <xsl:text> </xsl:text> </span>
        
        </xsl:when>
        
        <xsl:otherwise>
        
         <img style="{$s}" class="img-responsive" src="{$baseimg}{substring-after(f:getImageNameById($imageId), 'media/')}"
             alt="Immagine non visualizzabile" />
        
        </xsl:otherwise>
             
        </xsl:choose>     
        
    </xsl:template>

    <xd:doc scope="add.list">
        <xd:desc>
            <xd:p>This template is in charge of handling lists.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.list">
        <xsl:variable name="currentNumId"
                      as="xs:integer"
                      select="w:pPr/w:numPr/w:numId/@w:val"
        />
        <xsl:variable name="thisListItems"
                      as="element()*"
                      select="f:getListItems(.)"
        />
        <xsl:choose>
            <xsl:when test="f:isBullet($currentNumId)">
                <ul>
                
                <xsl:attribute name="id">
                	<xsl:value-of select="concat('list_',generate-id())"/>
                </xsl:attribute>
                
                    <xsl:call-template name="add.listItems">
                        <xsl:with-param name="listElements" as="element()*" select="$thisListItems"/>
                    </xsl:call-template>
                </ul>
            </xsl:when>
            <xsl:otherwise>
                <ol>
                
                <xsl:attribute name="id">
                	<xsl:value-of select="concat('list_',generate-id())"/>
                </xsl:attribute>
                
                    <xsl:call-template name="add.listItems">
                        <xsl:with-param name="listElements" as="element()*" select="$thisListItems"/>
                    </xsl:call-template>
                </ol>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="add.listParam">
        <xd:desc>
            <xd:p>This template is in charge of handling lists.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.listParam">
        <xsl:param name="el" as="element()"/>
        <xsl:variable name="currentNumId"
                      as="xs:integer"
                      select="$el/w:pPr/w:numPr/w:numId/@w:val"
        />
        <xsl:variable name="thisListItems"
                      as="element()*"
                      select="f:getListItems($el)"
        />
        <xsl:choose>
            <xsl:when test="f:isBullet($currentNumId)">
                <ul>
                
                <xsl:attribute name="id">
                	<xsl:value-of select="concat('list_',generate-id())"/>
                </xsl:attribute>
                
                    <xsl:call-template name="add.listItems">
                        <xsl:with-param name="listElements" as="element()*" select="$thisListItems"/>
                    </xsl:call-template>
                </ul>
            </xsl:when>
            <xsl:otherwise>
                <ol>
                
                <xsl:attribute name="id">
                	<xsl:value-of select="concat('list_',generate-id())"/>
                </xsl:attribute>
                
                    <xsl:call-template name="add.listItems">
                        <xsl:with-param name="listElements" as="element()*" select="$thisListItems"/>
                    </xsl:call-template>
                </ol>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="add.listItems" >
        <xd:desc>
            <xd:p>This template handles the list items</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.listItems">
        <xsl:param name="listElements" as="element()*" />
        <xsl:variable name="lastItem" as="element()?" select="$listElements[count($listElements)]" />
        <xsl:variable name="nextListItem" as="element()?" select="$lastItem/following-sibling::w:p[f:pIsListElement(.)][1]" />
        <xsl:variable name="lastParagraph" as="element()?" select="$lastItem/following-sibling::w:p[1][f:isLastListParagraph(.)]" />
        <xsl:variable name="lastParagraphs"
                      as="element()*"
                      select="
                        $lastParagraph,
                        ($lastParagraph/following-sibling::w:p[f:isLastListParagraph(.)] except $nextListItem/following-sibling::w:p[f:isLastListParagraph(.)])
                      "
        />
        <xsl:for-each select="$listElements">
            <xsl:variable name="currentLevel" as="xs:integer" select="./w:pPr/w:numPr/w:ilvl/@w:val" />
            <xsl:variable name="currId" as="xs:integer" select="./w:pPr/w:numPr/w:numId/@w:val" />
            <xsl:variable name="next" as="element()?" select="following-sibling::w:p[f:pIsListElement(.)][1]" />
            <xsl:variable name="nextParagraphsNotInList" as="element()*" select="following-sibling::w:p[not(f:pIsListElement(.))]" />
            <xsl:variable name="nextListItemIndex" as="xs:integer" select="position()+1" />
            <xsl:variable name="paragraphs"
                          as="element()*"
                          select="$nextParagraphsNotInList intersect $listElements[$nextListItemIndex]/preceding-sibling::w:p"
            />
            <xsl:variable name="isFirstNested"
                          as="xs:boolean"
                          select="
                            $currentLevel = $listElements[1]/w:pPr/w:numPr/w:ilvl/@w:val + 1
                            and (not(exists(./preceding-sibling::w:p[w:pPr/w:numPr/w:ilvl/@w:val = $currentLevel]))
                             ) 
                          "
            />
            <xsl:variable name="nextIsFirstNested"
                          as="xs:boolean"
                          select="
                            exists($next)
                            and $currentLevel = $next/w:pPr/w:numPr/w:ilvl/@w:val - 1 " />
                   <!--         and not(exists($next/preceding-sibling::w:p[(w:pPr/w:numPr/w:ilvl/@w:val = $currentLevel + 1) and (w:pPr/w:numPr/w:numId/@w:val=$currId)])) and" -->
            

            <!--<xsl:choose>-->
                <!--<xsl:when test="$isFirstNested">-->
                    <!--<xsl:call-template name="add.list" />-->
                <!--</xsl:when>-->
                <!--<xsl:otherwise>-->
            <xsl:if test="./w:pPr/w:numPr/w:ilvl/@w:val = $listElements[1]/w:pPr/w:numPr/w:ilvl/@w:val">
                <li>
              <!--      <xsl:call-template name="set.bookmarked.object.id" /> -->
                    <xsl:if test="some $content in $bibliography satisfies lower-case(normalize-space(preceding::w:p[f:isHeading(.)][1])) = $content">
                        <xsl:attribute name="role">doc-biblioentry</xsl:attribute>
                    </xsl:if>
                    <xsl:call-template name="add.p">
                        <xsl:with-param name="isInsideList" select="true()"/>
                        <xsl:with-param name="isInsideNote" select="true()"/>
                    </xsl:call-template>
                    <xsl:for-each select="$paragraphs">
                        <xsl:if test="f:containsText(.)">
                            <p>
                                <xsl:apply-templates />
                            </p>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:if test="count($listElements) = position()">
                        <xsl:for-each select="$lastParagraphs">
                            <xsl:if test="f:containsText(.)">
                                <p>
                                    <xsl:apply-templates />
                                </p>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="$nextIsFirstNested">
                        <xsl:call-template name="add.listParam">
                            <xsl:with-param name="el" select="$next" />
                        </xsl:call-template>
                    </xsl:if>
                </li>
            </xsl:if>
                <!--</xsl:otherwise>-->
            <!--</xsl:choose>-->
        </xsl:for-each>
    </xsl:template>

    <xd:doc scope="add.notes">
        <xd:desc>
            <xd:p>This template is in charge of handling references to footnotes.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.notes">
        <a href="#ftn{w:footnoteReference/@w:id}"><xsl:text> </xsl:text></a>
    </xsl:template>

    <xd:doc scope="add.footnotes">
        <xd:desc>
            <xd:p>This named template is in change of handling all the footnotes contained in the paper.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.footnotes">
        <xsl:if test="$footnotesExists">
            <!-- Select only the paragraphs containing text -->
            <xsl:variable name="footnoteElements" select="$footnotes//w:footnote[f:containsText(.)]" as="element()*" />
            <xsl:if test="exists($footnoteElements)">
                <section id="endnotes" role="doc-endnotes">
                    <xsl:for-each select="$footnoteElements">
                        <section id="ftn{./@w:id}" role="doc-endnote">
                            <xsl:apply-templates />
                        </section>
                    </xsl:for-each>
                </section>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xd:doc scope="get.following.content.elements">
        <xd:desc>
            <xd:p>This named template allow one to get all the content elements after the first one. Since this particular sequence of element is used by different templates (e.g., for headings), the call has been implemented as a named template.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="get.following.content.elements">
        <xsl:param name="curlev" select="1" as="xs:integer" />

        <xsl:variable name="seq" select="for $v in (1 to $curlev) return $v" as="xs:integer+" />
        <xsl:variable name="level" select="string($curlev)" as="xs:string" />
        <xsl:apply-templates select="(following-sibling::w:p[f:isHeading(.)])[some $l in $seq satisfies f:getLevel(.) = $l][1][f:getLevel(.) = $curlev]" />
    </xsl:template>

    <xd:doc scope="add.inline">
        <xd:desc>
            <xd:p>This named template is in change of handling all the inline textual elements that may appear within a paragraph. The links are handled in another appropriate template.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="add.inline">
        <xsl:param name="select" as="item()*" tunnel="yes" />
        <xsl:param name="s1" as="item()*" tunnel="yes" />
        <xsl:param name="bold" as="xs:boolean" tunnel="yes" />
        <xsl:param name="underline" as="xs:boolean" tunnel="yes" />
        <xsl:param name="italic" as="xs:boolean" tunnel="yes" />
        <xsl:param name="super" as="xs:boolean" tunnel="yes" />
        <xsl:param name="sub" as="xs:boolean" tunnel="yes" />
        <xsl:param name="note" as="xs:boolean" tunnel="yes" />
        <xsl:param name="image" as="xs:boolean" tunnel="yes" />
        <xsl:choose>
            <xsl:when test="$super and (some $i in $select satisfies $i[self::w:t])">
                <sup>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="super" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </sup>
            </xsl:when>
            
            <xsl:when test="$sub and (some $i in $select satisfies $i[self::w:t])">
                <sub>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="sub" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </sub>
            </xsl:when>
            
              <xsl:when test="$underline and (some $i in $select satisfies $i[self::w:t])">
                <u>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="underline" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </u>
            </xsl:when>
            
            <xsl:when test="$bold and (some $i in $select satisfies $i[self::w:t])">
                <b>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="bold" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </b>
            </xsl:when>
            <xsl:when test="$italic and (some $i in $select satisfies $i[self::w:t])">
                <i>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="italic" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </i>
            </xsl:when>
            <xsl:when test="$note and (some $i in $select satisfies $i[self::w:t])">
                <a href="#ftn{w:footnoteReference/@w:id}">
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="add.inline">
                        <xsl:with-param name="note" tunnel="yes" as="xs:boolean" select="false()" />
                    </xsl:call-template>
                </a>
            </xsl:when>
            <xsl:when test="$image">
            
            <xsl:for-each select="$s1">
                <xsl:call-template name="add.image.inline" />  
            </xsl:for-each> 
            
            </xsl:when>
            <xsl:otherwise>
            
            	<xsl:for-each select="$select">
            	
            		<xsl:if test="exists(./preceding-sibling::w:lastRenderedPageBreak) and (every $x in  (./ancestor::w:p) satisfies not(f:isHeading($x)))" >
            		
            		<span role="pdx-incrementer" data-pdx-family="page"><xsl:text> </xsl:text></span>
            		
            		</xsl:if>

                	<xsl:apply-templates select="f:getStringFromTextNodes(.)"/>

               </xsl:for-each> 
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="add.title">
        <xsl:variable name="title"
                      select="//w:p[f:getStyleName(.) = 'Title'][1]" as="element()?" />
        <xsl:variable name="subtitle"
                      select="//w:p[f:getStyleName(.) = 'Subtitle'][1]" as="element()?" />
        <title>
            <xsl:choose>
                <xsl:when test="normalize-space($title) != ''">
                    <xsl:value-of select="$title" />
                    <xsl:if test="normalize-space($subtitle) != ''">
                        <xsl:text> -- </xsl:text>
                        <xsl:value-of select="$subtitle" />
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>No title specified</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </title>
    </xsl:template>

    <xd:doc scope="set.section.type">
        <xd:desc>
            <xd:p>This named template set the type of a section if any ('section' is the default).</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="set.section.type">
        <xsl:variable name="content" select="lower-case(normalize-space(string-join(.//w:t, '')))" as="xs:string" />

        <xsl:choose>
            <xsl:when test="some $item in $abstract satisfies $content = $item">
                <xsl:attribute name="role" select="'doc-abstract'" />
            </xsl:when>
            <xsl:when test="some $item in $acknowledgements satisfies $content = $item">
                <xsl:attribute name="role" select="'doc-acknowledgements'" />
            </xsl:when>
            <xsl:when test="some $item in $bibliography satisfies $content = $item">
                <xsl:attribute name="role" select="'doc-bibliography'" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xd:doc scope="create.section.with.heading">
        <xd:desc>
            <xd:p>This named template creates the section according the particular heading in consideration.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="create.section.with.heading">
        <xsl:variable name="next.header" select="(following-sibling::w:p[f:isHeading(.)])[1]" as="element()*" />
    
        <xsl:variable name="level" select="f:getLevel(.)" as="xs:integer" />
        <section>
      
           <xsl:call-template name="set.bookmarked.object.id">
            	<xsl:with-param name="s" as="xs:boolean" select="true()" />
            </xsl:call-template> 
            <xsl:call-template name="set.section.type" />
            <h1>
            
            <xsl:if test="some $x in ./w:r satisfies exists($x[w:lastRenderedPageBreak])" >
            
            	<xsl:attribute name="data-pdx-pagebreakbefore" select="('true')" />

            </xsl:if>
            
            <span	role="pdx-hcounter">
            	<xsl:text> </xsl:text>
            </span>
             <xsl:text> </xsl:text>   <xsl:apply-templates />
            </h1>
            <xsl:apply-templates
                    select="(following-sibling::text()|following-sibling::element()) except
                $next.header/(.|following-sibling::text()|following-sibling::element())" /><!-- seleziona tutti i w:p fino a che non incontra il prossimo w:p che però non sia un header-->

            <!-- If the next header exists and has a higher level (it is starts a subsection), call it -->
            <xsl:if test="$next.header">
                <xsl:variable name="nextLevel" as="xs:integer" select="f:getLevel($next.header)" />
                <xsl:if test="$nextLevel > xs:integer($level)">
                    <xsl:apply-templates select="$next.header" />
                </xsl:if>
            </xsl:if>
        </section>
<!-- seleziona le sezioni che sono allo stesso livello di level -->
       
        <xsl:call-template name="get.following.content.elements"> 
            <xsl:with-param name="curlev" select="$level" />
        </xsl:call-template> 
    </xsl:template>

    <xd:doc scope="create.section.with.heading">
        <xd:desc>
            <xd:p>This named template creates a new section with no heading and containing the paragraph in consideration.</xd:p>
        </xd:desc>
    </xd:doc>
      <xsl:template name="create.untitled.section">
        <section>
            <h1>No heading specified</h1>
            <p>
                <xsl:apply-templates />
            </p>
            <xsl:apply-templates select="following-sibling::element()" />
        </section>
    </xsl:template>


    <xd:doc scope="set.bookmarked.object.id">
        <xd:desc>
            <xd:p>This named template set the id of a bookmarked object (i.e., section and references) if present.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="set.bookmarked.object.id">
    	  <xsl:param name="s" as="xs:boolean" select="false()" /> <!-- da mettere perchè la pdx cancella gli id e se c'è un riferimento viene perso vedere fixattr-js -->
        <xsl:variable name="ids" select=".//w:bookmarkStart/@w:name" as="xs:string*" />
        <xsl:variable name="instrTextIds" select="//w:instrText[not(.=preceding::*)]" as="xs:string*" />
        <xsl:variable name="id" as="xs:string*">
            <xsl:for-each select="$instrTextIds">
                <xsl:variable name="instrTextId" select="." as="xs:string" />
                <xsl:for-each select="$ids">
                
                <xsl:if test="starts-with(.,'_Ref')">
                    <xsl:if test="contains($instrTextId, .)">
                        <xsl:value-of select="." />
                    </xsl:if>
                 </xsl:if>  
                    
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        
        
        
         <xsl:if test="$id[1] and $id != '_GoBack'">
            <xsl:attribute name="id" select="$id[1]" />
         </xsl:if>
         
         <xsl:if test="$id[1] and $s">
            <xsl:attribute name="ab" select="$id[1]" />
         </xsl:if>
         
            
    </xsl:template>

    <xd:doc scope="set.captioned.object.id">
        <xd:desc>
            <xd:p>This named template set the id of a captioned object (i.e., a figure, a formula, a table) if present.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template name="set.captioned.object.id">
        <xsl:variable name="caption" as="element()?" select="following-sibling::w:p[1][f:pIsCaption(.)]" />
        <xsl:variable name="ids" select="$caption/w:bookmarkStart/@w:name" as="xs:string*" />
        <xsl:variable name="instrTextIds" select="//w:instrText[not(.=preceding::*)]" as="xs:string*" />
        <xsl:variable name="id" as="xs:string*">
            <xsl:for-each select="$instrTextIds">
                <xsl:variable name="instrTextId" select="." as="xs:string" />
                <xsl:for-each select="$ids">
                
                
                	<xsl:if test="starts-with(.,'_Ref')">
                		
                    <xsl:if test="contains($instrTextId, .)">
                        <xsl:value-of select="." />
                    </xsl:if>
                  </xsl:if>  
                  
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:if test="$id">
            <xsl:attribute name="id" select="$id[1]" />
            <xsl:attribute name="ab-fig" select="$id[1]" />
        </xsl:if>
    </xsl:template>
    
   


    <!-- FUNCTIONS -->

    <xd:doc scope="f:isRef">
        <xd:desc>
            <xd:p>This function says whether or not a given element is  a Ref.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isRef">
        <xsl:param name="r" as="element()" />
        <xsl:value-of
                select="
                    exists($r/w:fldChar)
                    and $r/w:fldChar/@w:fldCharType = 'begin'
                    and contains($r/following-sibling::w:r[1]/w:instrText/text(), 'REF')
                "
        />
    </xsl:function>

    <xd:doc scope="f:isInsideRef">
        <xd:desc>
            <xd:p>This function says whether or not a given w:r is inside a Ref.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isInsideRef">
        <xsl:param name="r" as="element()?" /><!--mod originale senza ? -->
        <xsl:value-of
                select="
                    matches($r/w:fldChar/@w:fldCharType, 'begin|separate|end')
                    or (
                        matches($r/preceding-sibling::w:r[w:fldChar][1]/w:fldChar/@w:fldCharType, 'begin|separate')
                        and matches($r/following-sibling::w:r[w:fldChar][1]/w:fldChar/@w:fldCharType, 'separate|end')
                    )
                "
        />
    </xsl:function>

    <xd:doc scope="f:listType">
        <xd:desc>
            <xd:p>This function returns the type (bullet or decimal) of a list given its ID.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:listType" as="xs:string">
        <xsl:param name="listId" as="xs:integer"/>
        <xsl:variable name="abstractNumId"
                      as="xs:integer"
                      select="$numbering//w:num[@w:numId = $listId]/w:abstractNumId/@w:val"
        />
        <xsl:variable name="abstractNumProps"
                      as="element()"
                      select="$numbering//w:abstractNum[@w:abstractNumId = $abstractNumId]"
        />
        <xsl:variable name="listType"
                      as="xs:string"
                      select="$abstractNumProps/w:lvl[@w:ilvl = 0]/w:numFmt/@w:val"
        />
        <xsl:value-of select="$listType"/>
    </xsl:function>

    <xd:doc scope="f:isBullet">
        <xd:desc>
            <xd:p>This function says whether or not a list element is bulleted. If it isn't bulleted then it's numbered.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isBullet" as="xs:boolean">
        <xsl:param name="listId" as="xs:integer"/>
        <xsl:variable name="isBullet"
                      as="xs:boolean"
                      select="f:listType($listId) = 'bullet'"
        />
        <xsl:value-of select="$isBullet"/>
    </xsl:function>

    <xd:doc scope="f:getContentChildElements">
        <xd:desc>
            <xd:p>This function returns the elements contained in a given element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getContentChildElements" as="element()*">
        <xsl:param name="parent" as="element()" />
        <xsl:sequence select="$parent/element()" />
    </xsl:function>

    <xd:doc scope="f:getFollowingInlines">
        <xd:desc>
            <xd:p>This function returns the following elements which are w:r or w:hyperlinks.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getFollowingInlines" as="element()*">
        <xsl:param name="curel" as="element()*" />
        <xsl:sequence select="$curel/(following-sibling::w:r|following-sibling::w:hyperlink)" />
    </xsl:function>

    <xd:doc scope="f:getPrecedingInlines">
        <xd:desc>
            <xd:p>This function returns the preceding elements which are w:r or w:hyperlinks.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getPrecedingInlines" as="element()*">
        <xsl:param name="curel" as="element()*" />
        <xsl:sequence select="$curel/(preceding-sibling::w:r|preceding-sibling::w:hyperlink)" />
    </xsl:function>

    <xd:doc scope="f:getLevel">
        <xd:desc>
            <xd:p>This function retrieves the level of a particular logic section of a paper starting from its header element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getLevel" as="xs:integer">
        <xsl:param name="curel" as="element()" />
        <xsl:variable name="value" as="xs:string">
            <xsl:variable name="title-ref" select="$curel/w:pPr/w:pStyle/@w:val" as="xs:string" />
            <xsl:value-of select="$styles//w:style[@w:styleId = $title-ref]/w:name/@w:val" />
        </xsl:variable>
        <xsl:value-of select="xs:integer(substring($value,string-length($value)))" />
    </xsl:function>

    <xd:doc scope="f:isHeading">
        <xd:desc>
            <xd:p>This function checks if a certain paragraph is an header or not.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isHeading" as="xs:boolean">
        <xsl:param name="curel" as="element()" />
        <xsl:variable name="value" as="xs:string?">
            <xsl:variable name="title-ref" select="$curel/w:pPr/w:pStyle/@w:val" as="xs:string?" />
            <xsl:value-of select="$styles//w:style[@w:styleId = $title-ref]/w:name/@w:val" />
        </xsl:variable>
        <xsl:value-of select="starts-with($value, 'heading')" />
    </xsl:function>

    <xd:doc scope="f:getLocalizedStyleName">
        <xd:desc>
            <xd:p>This function returns the localized (in the language of your Word installation) name of a given element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getLocalizedStyleName" as="xs:string">
        <xsl:param name="element" as="element()?" />
        <xsl:choose>
         <xsl:when test="exists($element//w:tblStyle)">
                <xsl:value-of select="string($element/w:tblPr/w:tblStyle/@w:val)"/>
            </xsl:when>
            <xsl:when test="exists($element//w:pStyle)">
                <xsl:value-of select="string($element/w:pPr/w:pStyle/@w:val)"/>
            </xsl:when>
           
            <xsl:otherwise>
                <xsl:value-of select="string($element/w:rPr/w:rStyle/@w:val)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xd:doc scope="f:getStyleName">
        <xd:desc>
            <xd:p>This function returns the style name of a given element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getStyleName" as="xs:string">
        <xsl:param name="element" as="element()?" />
        <xsl:value-of select="string($styles//w:style[@w:styleId = f:getLocalizedStyleName($element)]/w:name/@w:val)"/>
    </xsl:function>

    <xd:doc scope="f:type">
        <xd:desc>
            <xd:p>This function says whether or not a particular paragraph is a note to author or an example.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:type" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:param name="typ" as="xs:string" />
        <xsl:value-of select="f:getStyleName($element) = $typ"/>
    </xsl:function>
    

    <xd:doc scope="f:isLastListParagraph">
        <xd:desc>
            <xd:p>This function says whether or not a paragraph is contained in a list.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isLastListParagraph" as="xs:boolean">
        <xsl:param name="element" as="element()?" />
        <xsl:value-of select="f:getStyleName($element) = 'List Paragraph' and not(exists($element/w:pPr/w:numPr))" />
    </xsl:function>

    <xd:doc scope="f:pIsListElement">
        <xd:desc>
            <xd:p>This function says whether or not a paragraph is contained in a list.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:pIsListElement" as="xs:boolean">
        <xsl:param name="element" as="element()?" />

          <xsl:value-of select=" exists($element/w:pPr/w:numPr/w:ilvl)
            and exists($element/w:pPr/w:numPr/w:numId)
            and not(f:isHeading($element))
            "
        />
        
    </xsl:function>

    <xd:doc scope="f:isNote">
        <xd:desc>
            <xd:p>This function says whether or not an element is a reference to a footnote.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isNote" as="xs:boolean">
        <xsl:param name="element" as="element()?" />
        <xsl:variable name="hasNoteStyle" as="xs:boolean" select="f:getStyleName($element) = 'footnote reference'" />
        <xsl:variable name="containsFootnoteRef" as="xs:boolean" select="exists($element/descendant-or-self::w:footnoteReference)" />
        <xsl:value-of select="$hasNoteStyle and $containsFootnoteRef"/>
    </xsl:function>

    <xd:doc scope="f:pIsListingBox">
        <xd:desc>
            <xd:p>This function says whether or not a paragraph is a listing box.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:pIsListingBox" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:value-of select="exists($element/w:r/mc:AlternateContent/mc:Choice/w:drawing/wp:inline/a:graphic/a:graphicData/wps:wsp/wps:txbx)"/>
    </xsl:function>

    <xd:doc scope="f:getListItems">
        <xd:desc>
            <xd:p>This function returns the elements of a list given its first element.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getListItems" as="element()*">
        <xsl:param name="firstListItem" as="element()" />
        <xsl:variable name="currentNumId"
                      as="xs:integer"
                      select="$firstListItem/w:pPr/w:numPr/w:numId/@w:val"
        />
        
        <xsl:variable name="nh" as="element()?" select="$firstListItem/following-sibling::w:p[f:isHeading(.)][1]"/>
        <xsl:variable name="ne" as="element()*" select="$nh/following-sibling::w:p"/>
        <xsl:variable name="nextListsElements"
                      as="element()*"
                      select="$firstListItem/following-sibling::w:p[f:pIsListElement(.)]"
        />
        <xsl:variable name="firstElementOfNextList"
                      as="element()?"
                      select="$firstListItem/following-sibling::w:p[f:pIsListElement(.)][w:pPr/w:numPr/w:numId/@w:val != $currentNumId][1]"
        />
        <xsl:variable name="nextListsElementsNotInTheSameList"
                      as="element()*"
                      select="$firstElementOfNextList/following-sibling::w:p[f:pIsListElement(.)]"
        />
        <xsl:variable name="listsElementsOfAnUpperLevel"
                      as="element()*"
                      select="$firstListItem/following-sibling::w:p[w:pPr/w:numPr/w:ilvl/@w:val &lt; $firstListItem/w:pPr/w:numPr/w:ilvl/@w:val]"
        />
        
        <xsl:variable name="nexth" as="element()*" select="$listsElementsOfAnUpperLevel[1]/following-sibling::w:p" />
        
        <xsl:variable name="thisListItems"
                      as="element()*"
                      select="($firstListItem , $nextListsElements) except ($firstElementOfNextList, $nextListsElementsNotInTheSameList, $listsElementsOfAnUpperLevel,$nh,$ne,$nexth)"
        />
        <xsl:sequence select="$thisListItems"/>
    </xsl:function>

    <xd:doc scope="f:isFirstItemOfAList">
        <xd:desc>
            <xd:p>Return whether or not an element is the first item of a list.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:isFirstItemOfAList" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:variable name="precedingItem"
                      as="element()?"
                      select="$element/preceding-sibling::w:p[f:pIsListElement(.)][1]"
        />
        <xsl:value-of select="
                    (
                        not(exists($precedingItem))
                        or ($precedingItem/w:pPr/w:numPr/w:numId/@w:val != $element/w:pPr/w:numPr/w:numId/@w:val)
                        or ($precedingItem/w:pPr/w:numPr/w:ilvl/@w:val = $element/w:pPr/w:numPr/w:ilvl/@w:val - 1)
                        or (some $i in ($precedingItem/following-sibling::w:p except $element/following-sibling::w:p) satisfies f:isHeading($i))
                    )
                    and $element/w:pPr/w:numPr/w:ilvl/@w:val = 0
            "
        />
    </xsl:function>

    <xd:doc scope="f:pIsBetweenTwoListItems">
        <xd:desc>
            <xd:p>This function says whether or not a paragraph is after a list item.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:pIsBetweenTwoListItems" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:variable name="previousItem" as="element()?" select="$element/preceding-sibling::w:p[f:pIsListElement(.)][1]" />
        <xsl:variable name="nextItem" as="element()?" select="$element/following-sibling::w:p[f:pIsListElement(.)][1]" />
        
        <xsl:value-of select="$previousItem/w:pPr/w:numPr/w:numId/@w:val = $nextItem/w:pPr/w:numPr/w:numId/@w:val
        and ( every $i in ($previousItem/following-sibling::w:p except $nextItem/following-sibling::w:p) satisfies not(f:isHeading($i)) )" />
    </xsl:function>

    <xd:doc scope="f:pIsCaption">
        <xd:desc>
            <xd:p>Return whether or not a w:p element is a caption.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:pIsCaption" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:variable name="previousItem"
                      as="element()?"
                      select="$element/preceding-sibling::w:p[1]"
        />

        <xsl:variable name="isCaption"
                      as="xs:boolean"
                      select="f:getStyleName($element) = 'caption'"
        />
        <xsl:value-of select="$isCaption and exists($previousItem)"/>
    </xsl:function>

    <xd:doc scope="f:containsAnImage">
        <xd:desc>
            <xd:p>Return whether an element contains an image.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:containsAnImage" as="xs:boolean">
        <xsl:param name="element" as="element()?" />
        <xsl:variable name="image"
                      as="element()*"
                      select="$element/descendant::pic:pic[pic:blipFill/a:blip][not(ancestor::mc:Fallback)]"
        />
        <xsl:value-of select="exists($image)"/>
    </xsl:function>

    <xd:doc scope="f:getImageNameById">
        <xd:desc>
            <xd:p>Return an image filename given its ID.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getImageNameById" as="xs:string" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
        <xsl:param name="id" as="xs:string" />
        <xsl:variable name="imageName"
                      as="xs:string"
                      select="string($links//Relationship[@Id = $id]/@Target)"
        />
        <xsl:value-of select="$imageName"/>
    </xsl:function>

    <xd:doc scope="f:containsText">
        <xd:desc>
            <xd:p>Return whether or not an element contains w:t elements in its descendings.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:containsText" as="xs:boolean">
        <xsl:param name="element" as="element()" />
        <xsl:value-of select="exists($element//w:t)" />
    </xsl:function>

    <xd:doc scope="f:nodeContainsANumber">
        <xd:desc>
            <xd:p>Return whether or not a node contains a number in its descending text nodes.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:nodeContainsANumber" as="xs:boolean">
        <xsl:param name="node" as="node()" />
        <xsl:variable name="nodeText"
                      as="xs:string"
                      select="string($node//text())"
        />
        <xsl:value-of select="if (some $i in (0 to 9) satisfies contains($nodeText, string($i))) then true() else false()" />
    </xsl:function>

    <xd:doc scope="f:getCaptionNodes">
        <xd:desc>
            <xd:p>This functions given a paragraph returns the nodes containing its caption. Should be used only by getCaptionText.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getCaptionNodes" as="text()*">
        <xsl:param name="paragraph" as="element()" />
        <xsl:variable name="nodes"
                      as="element()*"
                      select="$paragraph/following::w:p[1][f:pIsCaption(.)]/descendant::w:t"
        />
        <xsl:variable name="firstNodeContainingANumber"
                      as="element()?"
                      select="$nodes[f:nodeContainsANumber(.)][1]"
        />
        <xsl:choose>
            <xsl:when test="exists($firstNodeContainingANumber)">
                <xsl:variable name="firstNodeContainingANumberIndex"
                              as="xs:integer"
                              select="index-of($nodes, $firstNodeContainingANumber)"
                />
                <xsl:value-of select="subsequence($nodes, $firstNodeContainingANumberIndex + 1)" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$nodes" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xd:doc scope="f:getCaptionText">
        <xd:desc>
            <xd:p>This function given a paragraph returns its caption.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getCaptionText" as="xs:string">
        <xsl:param name="paragraph" as="element()" />
        <xsl:variable name="nodes" as="text()*" select="f:getCaptionNodes($paragraph)" />
        <xsl:variable name="caption" as="xs:string" select="string($nodes)" />
        <xsl:choose>
            <xsl:when test="matches($caption, '^(\.|,|:|;)')">
                <xsl:value-of select="normalize-space(substring($caption, 2))" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$caption" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xd:doc scope="f:sequenceOfTextNodes">
        <xd:desc>
            <xd:p>This function returns a sequence of text nodes split by means of double quotations.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:sequenceOfTextNodes" as="xs:string*">
        <xsl:param name="curtext" as="xs:string" />
        <xsl:param name="curseq" as="xs:string*" />
        <xsl:choose>
            <xsl:when test="contains($curtext, '“') and contains($curtext, '”')">
                <xsl:sequence select="f:sequenceOfTextNodes(
                    substring-after($curtext, '”'),
                    (
                        $curseq,
                        substring-before($curtext,'“'),
                        substring-after(substring-before($curtext, '”'),'“')))" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="$curseq, $curtext" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>


    <xd:doc scope="f:getStringFromTextNodes">
        <xd:desc>
            <xd:p>Returns the string contained in the given nodes.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:function name="f:getStringFromTextNodes">
        <xsl:param name="nodes" as="node()*" />
        <xsl:variable name="n" as="xs:integer" select="count($nodes)" />
     
        
        <xsl:value-of select="if ($n > 1) then concat(f:getStringFromTextNodes($nodes except $nodes[$n]), string($nodes[$n]//text())) else string($nodes[1])" />
    </xsl:function>
 <!-- ======================== -->
	
	<xsl:function name="f:TitleEl" as="xs:string" >
	
		<xsl:param name="cel" as="element()?"/>
		<xsl:variable name="predel"  as="element()?" select="($cel/preceding-sibling::w:p[f:isHeading(.)])[last()]"/>
		<xsl:value-of select="lower-case(normalize-space(f:getStringFromTextNodes($predel)))" />
	</xsl:function>	

</xsl:stylesheet>
