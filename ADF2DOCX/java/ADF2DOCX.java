package andrea;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.concurrent.ThreadLocalRandom;

import net.sf.saxon.TransformerFactoryImpl;

public class ADF2DOCX {

	static int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
	static String OUTPUT_FL="tmp/";
	static String OUTPUT_FLD = OUTPUT_FL.concat(Integer.toString(randomNum));
	static String OUTPUT_file;
	static String INPUT_FLD;
	static String INPUT_file;
	static String STYLE;
	static boolean StatusStyle = false;
	static boolean Content = false;

	public static void main(String [] args) throws IOException , IllegalArgumentException {
		
		Options options = new Options();
		options.addOption("i", true, "specify the input file");
		options.addOption("o", true, "specify the output file");
		options.addOption("style", true, "specify the style sheet");
		options.addOption("content", false, "for convert only content without FrontMatter, CoverPage, To*");
		options.addOption("h", false, "print this message");
		CommandLineParser parser = new DefaultParser();
		
		try{
			CommandLine cmd = parser.parse(options, args);

			
			if (cmd.hasOption("h")) {
				System.out.println("\n-i  <file>  MANDATORY - The input .html document.\n");
				System.out.println("-o  <file>  MANDATORY - The file where the output (.docx) file should be stored.\n");
				System.out.println("-style  <file>  OPTIONAL - specify the style sheet that should be applied, with name styles.xml ,which must be supplied in input \n");
				System.out.println("-content  OPTIONAL - for convert only content without FrontMatter, CoverPage, To* \n");
				System.out.println("-h  Print this message.\n");
				System.exit(1);
			    
			}

			
			if (cmd.hasOption("i")) {
				
				INPUT_file = cmd.getOptionValue("i");
				
				File input = new File(INPUT_file);
				
				if(input.isDirectory()  ){
					
					 throw new IllegalArgumentException("The provided file is a directory ");
					
				}
				
				if(!input.exists()  ){
					
					 throw new IllegalArgumentException("The provided file doesn't exist");
					
				}
				
				if(! INPUT_file.endsWith(".html") ){
					
					 throw new IllegalArgumentException("The provided file doesn't end with .html");
					
				}
				
			/*	if(INPUT_file.substring(INPUT_file.lastIndexOf(input.getName()), INPUT_file.lastIndexOf(".html")).length() == 0  ){
						throw new IllegalArgumentException("Error: the provided name for Input File: " +INPUT_file + " doesn't have a name before .html");

				}*/
				
				
				
				
			} else {
				//log.log(Level.SEVERE, "Missing -i option");
			    throw new IllegalArgumentException("Error: Missing -i option");
			}
			
			
			if (cmd.hasOption("o")){
				
				OUTPUT_file= cmd.getOptionValue("o");
				
				
			//	File f2= new File(OUTPUT_file.concat(".docx"));
				File f2= new File(OUTPUT_file);
				
				if ( ! OUTPUT_file.endsWith(".docx")){
					
					throw new IllegalArgumentException("Error: the provided name for Output File: " +OUTPUT_file + " doesn't end with .docx, specify a name that ends with .docx");
					
				}
				
				 if(OUTPUT_file.substring(OUTPUT_file.lastIndexOf(f2.getName()), OUTPUT_file.lastIndexOf(".docx")).length() == 0  ){
					throw new IllegalArgumentException("Error: the provided name for Output File: " +OUTPUT_file + " doesn't have a name before .docx");

				}
				
				
				 	if( f2.exists() ){
					
					throw new IllegalArgumentException("Error: File name specified  for output already exists, try with another name please.");
					
				}
				
				
				if(f2.getParent() != null ){
					new File(f2.getParent()).mkdirs();
				}
				
				
			
				
			/*	else if (OUTPUT_file.contains(".")){
					
					throw new IllegalArgumentException("Error: the provided name for output file: " + OUTPUT_file + " contains a dot please specify a name without a dot");
					
				}
				
				else{
					
					OUTPUT_file=OUTPUT_file.concat(".docx");
					
				}*/
				
			}
			
			
			
			else{
				
				throw new IllegalArgumentException("Error: Missing -o option");
				
			}
			
			
			
			if (cmd.hasOption("style")){
				
				STYLE= cmd.getOptionValue("style");
				
				File styleSheet= new File(STYLE);
				
				if (! styleSheet.exists() || ! StringUtils.equals(styleSheet.getName(), "styles.xml") || styleSheet.isDirectory()){
					
					throw new IllegalArgumentException("Error: Invalid supplied styleSheet");
					
				}
				
				StatusStyle= true;
				
			}
			
			
			if (cmd.hasOption("content")){
				
				Content = true;
				
			}
			
			
			
		
		}
		
		catch(MissingArgumentException e){
		System.out.println(	e.getLocalizedMessage());
		System.exit(1);
		}
		
		catch (ParseException e) {
			System.out.println("Failed to parse command line properties");
			System.exit(1);
			
		}
		
		catch(IllegalArgumentException e){
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		}
		
		catch(Exception e){
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		}
		
		
		try{
			File folder = new File(OUTPUT_FLD);
	       	if(!folder.exists()) {
	       		folder.mkdir();
	       	}
	/*       	File tmp = new File("ris.docx");
	       	if (tmp.exists()){
	       		FileUtils.deleteQuietly(tmp);
	       	}*/
	       	
	       	ArrayList<String> wordfile = new ArrayList<String>();
	       //	wordfile.add("numbering.xml");
			wordfile.add("fontTable.xml");
			wordfile.add("settings.xml");
			if(! StatusStyle){
				wordfile.add("styles.xml");
			}
			//wordfile.add("stylesWithEffects.xml");
			
			wordfile.add("webSettings.xml");
			wordfile.add("endnotes.xml");
			wordfile.add("footnotes.xml");
	       	
			for (String file : wordfile) {
				InputStream is = ADF2DOCX.class.getClassLoader().getResourceAsStream("word/" + file);
				FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "word" + "/" + file));	
			}
			
			if(StatusStyle){
				FileUtils.copyFileToDirectory(new File(STYLE),new File(OUTPUT_FLD + "/" + "word" + "/") );
			}
			
			
			ArrayList<String> worddir = new ArrayList<String>();
			worddir.add("theme1.xml");
			
			for (String file : worddir) {
				InputStream is = ADF2DOCX.class.getClassLoader().getResourceAsStream("word/theme/" + file);
				FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "word" + "/" + "theme"+ "/" +file));	
			}
			
			
			/*
			ArrayList<String> worddir2 = new ArrayList<String>();
			worddir2.add("document.xml.rels");
			
			for (String file : worddir2) {
				InputStream is = ProvaZip.class.getClassLoader().getResourceAsStream("word/_rels/" + file);
				FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "word" + "/" + "_rels"+ "/" +file));	
			}*/
		/*	
			ArrayList<String> docProps = new ArrayList<String>();
			docProps.add("app.xml");
			docProps.add("core.xml");
			
			for (String file : docProps) {
				InputStream is = ADF2DOCX.class.getClassLoader().getResourceAsStream("docProps/" + file);
				FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "docProps" + "/" +file));	
			}*/
			
			ArrayList<String> rel = new ArrayList<String>();
			rel.add(".rels");
			
			
			for (String file : rel) {
				InputStream is = ADF2DOCX.class.getClassLoader().getResourceAsStream("_rels/" + file);
				FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "_rels" + "/" +file));	
			}
			
			
			InputStream is = ADF2DOCX.class.getClassLoader().getResourceAsStream("[Content_Types].xml");
			FileUtils.copyInputStreamToFile(is, new File(OUTPUT_FLD + "/" + "[Content_Types].xml" ));
	    //	System.out.println(new File(OUTPUT_FLD).getAbsolutePath().toString());
			
		/*	File pictureDir = new File(INPUT_FLD + "/" + "img");
			
			if (pictureDir.exists()){
				
				File imgWithoutSpace = new File(INPUT_FLD + "/" + "imgWithoutSpace");
				
				if( imgWithoutSpace.exists()){
					
					FileUtils.deleteQuietly(imgWithoutSpace);
				}
				
				FileUtils.copyDirectory(pictureDir,imgWithoutSpace);
				
				File [] arr = imgWithoutSpace.listFiles();
				
				for(File f : arr){
					
					if ( ! StringUtils.equals(f.getName(), StringUtils.deleteWhitespace(f.getName()) ) )
					{
						System.out.println("non");
						File parent=new File(f.getParent());
						File n = new File(parent.getAbsolutePath().toString()+ "/" +StringUtils.deleteWhitespace(f.getName()));
						f.renameTo(n);	
					
					}
					
				}
				
			
				FileUtils.copyDirectory(imgWithoutSpace, new File( OUTPUT_FLD + "/" + "word" + "/" + "media"));
				
			}*/
			
			
			//prova img
			
			
			File input = new File(INPUT_file);
			Document doc = Jsoup.parse(input, "UTF-8", "");
			//Elements links = doc.getElementsByTag("img");
			Elements links;
			
			if (Content){
				
				links = doc.select("section[id='content'] img");
				
			}
			else{
				
				links = doc.select("img");
				
			}
		
			
			for (Element link : links) {
					String src=link.attr("src");
					String p = input.getParent();
					String sour;
					if(p!=null){
						sour=p + "/";
						
					}
					else{
						sour="";
					}
					
					File testImg= new File(sour + src);
					
					if (! testImg.exists()){
						
						throw new FileNotFoundException("Errore: L'immagine : "+ sour + src +" non è stata trovata" );
						
					}
					
					FileUtils.copyFileToDirectory(new File (sour + src), new File(OUTPUT_FLD + "/" + "word" + "/" + "media"));
					
					
				}
			File media= new File(OUTPUT_FLD + "/" + "word" + "/" + "media");
			
			if(media.exists()){
			
				for(String f : media.list()){
				
					if ( ! StringUtils.equals(f, StringUtils.deleteWhitespace(f) )){
					//	System.out.println("non uguale");
						Files.move(new File(OUTPUT_FLD + "/" + "word" + "/" + "media"+"/"+f).toPath(), new File(OUTPUT_FLD + "/" + "word" + "/" + "media"+"/"+StringUtils.deleteWhitespace(f) ).toPath(),REPLACE_EXISTING);
						
					}
					
				///	Files.move(new File("media"+"/"+f).toPath(), new File("media"+"/"+StringUtils.deleteWhitespace(f) ).toPath());
				
			
			//	System.out.println(f);
				}
			
			
			
			

			}

			//prova img

			
			try {
				
				
			    TransformerFactory tFactory0 = TransformerFactoryImpl.newInstance("net.sf.saxon.TransformerFactoryImpl", null);

		        StreamSource xslStream0 = new javax.xml.transform.stream.StreamSource(ADF2DOCX.class.getClassLoader().getResourceAsStream("rel.xsl"));
		        Transformer transformer0 =tFactory0.newTransformer(xslStream0);
		        new File(OUTPUT_FLD + "/" +"word"+ "/"+"_rels"+"/").mkdirs();
		        transformer0.setParameter("dir", OUTPUT_FLD + "/" +"word"+ "/"+"_rels"+"/");
		        
		        if(Content){
			        transformer0.setParameter("content", true);
			        }

		        FileInputStream temp01 = new FileInputStream(INPUT_file);
		        FileOutputStream temp02 = new FileOutputStream(OUTPUT_FLD + "/" +"word"+ "/"+"_rels"+"/"+"document.xml.rels");
		            
		        transformer0.transform(
		         new javax.xml.transform.stream.StreamSource(temp01),
		         new javax.xml.transform.stream.StreamResult( temp02)
		          );
		   /*         File tmp = new File("esempio");
		            if (tmp .exists() && tmp.isDirectory()){
		            	FileUtils.deleteQuietly(tmp);
		            }*/
		        temp01.close();
		        temp02.close();
		     /*   FileUtils. moveFileToDirectory(new File("document.xml.rels"),new File(OUTPUT_FLD+"/"+"word"+"/"+"_rels"),true);
		        
		        if(new File("header1.xml.rels").exists()){
		        	
		        	FileUtils. moveFileToDirectory(new File("header1.xml.rels"),new File(OUTPUT_FLD+"/"+"word"+"/"+"_rels"),true);
		        	
		        }
				
		        if(new File("footer1.xml.rels").exists()){
		        	
		        	FileUtils. moveFileToDirectory(new File("footer1.xml.rels"),new File(OUTPUT_FLD+"/"+"word"+"/"+"_rels"),true);
		        	
		        }
		        */
		        
		        
		        TransformerFactory tFactorynumb = TransformerFactoryImpl.newInstance("net.sf.saxon.TransformerFactoryImpl", null);

		        StreamSource xslStreamnumb = new javax.xml.transform.stream.StreamSource(ADF2DOCX.class.getClassLoader().getResourceAsStream("numb.xsl"));
		        Transformer transformernumb =tFactorynumb.newTransformer(xslStreamnumb);
		       
	

		        FileInputStream temp01numb = new FileInputStream(INPUT_file);
		        new File(OUTPUT_FLD + "/" +"word"+ "/").mkdirs();
		        FileOutputStream temp02numb = new FileOutputStream(OUTPUT_FLD + "/" +"word"+ "/"+"numbering.xml");
		            
		        transformernumb.transform(
		         new javax.xml.transform.stream.StreamSource(temp01numb),
		         new javax.xml.transform.stream.StreamResult( temp02numb)
		          );
		   /*         File tmp = new File("esempio");
		            if (tmp .exists() && tmp.isDirectory()){
		            	FileUtils.deleteQuietly(tmp);
		            }*/
		        temp01numb.close();
		        temp02numb.close();
		   //     FileUtils. moveFileToDirectory(new File("numbering.xml"),new File(OUTPUT_FLD+"/"+"word"),true);
		           
		        
		        
		        
		        
		        
				

	            TransformerFactory tFactory = TransformerFactoryImpl.newInstance("net.sf.saxon.TransformerFactoryImpl", null);

	            StreamSource xslStream = new javax.xml.transform.stream.StreamSource(ADF2DOCX.class.getClassLoader().getResourceAsStream("fromadf2docx.xsl"));
	            Transformer transformer =
	                    tFactory.newTransformer(xslStream);
	            new File(OUTPUT_FLD + "/" +"word"+ "/").mkdirs();
	            transformer.setParameter("dir", OUTPUT_FLD + "/" +"word"+ "/");
	            if(Content){
			        transformer.setParameter("content", true);
			        }

	            FileInputStream temp1 = new FileInputStream(INPUT_file);
	            FileOutputStream temp2 = new FileOutputStream(OUTPUT_FLD + "/" +"word"+ "/"+"document.xml");
	            
	            transformer.transform(
	                    new javax.xml.transform.stream.StreamSource(temp1),
	                    new javax.xml.transform.stream.StreamResult( temp2)
	            );
	   /*         File tmp = new File("esempio");
	            if (tmp .exists() && tmp.isDirectory()){
	            	FileUtils.deleteQuietly(tmp);
	            }*/
	            temp1.close();
	            temp2.close();
	         //  FileUtils. moveFileToDirectory(new File("document.xml"),new File(OUTPUT_FLD+"/"+"word"),true);
	         //  FileUtils. moveFileToDirectory(new File("header1.xml"),new File(OUTPUT_FLD+"/"+"word"),true);
	         //  FileUtils. moveFileToDirectory(new File("footer1.xml"),new File(OUTPUT_FLD+"/"+"word"),true);
	           
	           
	           
	           
	           
			} catch (Exception e) {
				
	            e.printStackTrace( );
	            System.exit(1);
			}
			
			
			

			
			
			//listFilesForFolder(new File(OUTPUT_FLD) );
			ZipIt appZip=new ZipIt(new File(OUTPUT_FLD).getAbsolutePath().toString(),OUTPUT_file);
			
	    	appZip.generateFileList(new File(ZipIt.SOURCE_FOLDER));
	    	appZip.zipIt(ZipIt.OUTPUT_ZIP_FILE);
	    	
	    	File tempDir = new File(OUTPUT_FLD);
	    	
	    	if (tempDir.exists())
				FileUtils.deleteDirectory(tempDir);
			
			}
		
		
	
		
			catch(FileNotFoundException e){
				
				File del = new File (OUTPUT_FLD);
				
				if (del.exists() && del.isDirectory()){
					
					FileUtils.deleteDirectory(del);
		
				}
				
				System.out.println(e.getLocalizedMessage());		
				
				System.exit(1);
				
				
			}
		
		catch(IOException e){
			
			System.out.println(e.getLocalizedMessage());
			
			System.exit(1);
			
		}
		
		catch(Exception e){
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		}
		
		
			

	}
	
	public static void listFilesForFolder(final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	            System.out.println(fileEntry.getAbsolutePath());
	        }
	    }
	}
	
	

}
