<?xml version="1.0" encoding="UTF-8" standalone="yes"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
xmlns:f="http://my/XSLT/function"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex" 
xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex" 
xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex" 
xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex" 
xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex" 
xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex" 
xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex" 
xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex" 
xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex" 
xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink" 
xmlns:o="urn:schemas-microsoft-com:office:office" 
xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" 
xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" 
xmlns:v="urn:schemas-microsoft-com:vml" 
xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" 
xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" 
xmlns:w10="urn:schemas-microsoft-com:office:word" 
xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" 
xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" 
xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" 
xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" 
xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" 
xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" 
xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" 
xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" 
mc:Ignorable="w14 w15 w16se wp14" 
xpath-default-namespace="http://www.w3.org/1999/xhtml"
exclude-result-prefixes="f xs "> <!-- xpath-default-namespace="http://www.w3.org/1999/xhtml"xpath-default-namespace="http://www.w3.org/1999/xhtml" -->

<xsl:template match="/" >

<xsl:variable name="DefaultInd" as="element()*" >
<w:num w:numId="1">
	<w:abstractNumId w:val="0"/>
</w:num>
</xsl:variable>



<xsl:variable name="DefaultAbstract" as="element()*" >
<w:abstractNum w:abstractNumId="0" w15:restartNumberingAfterBreak="0">
		<w:nsid w:val="06EA6181"/>
		<w:multiLevelType w:val="multilevel"/>
		<w:tmpl w:val="04090025"/>
		<w:lvl w:ilvl="0">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo1"/>
			<w:lvlText w:val="%1"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="432" w:hanging="432"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="1">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo2"/>
			<w:lvlText w:val="%1.%2"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="576" w:hanging="576"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="2">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo3"/>
			<w:lvlText w:val="%1.%2.%3"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="720" w:hanging="720"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="3">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo4"/>
			<w:lvlText w:val="%1.%2.%3.%4"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="864" w:hanging="864"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="4">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo5"/>
			<w:lvlText w:val="%1.%2.%3.%4.%5"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="1008" w:hanging="1008"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="5">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo6"/>
			<w:lvlText w:val="%1.%2.%3.%4.%5.%6"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="1152" w:hanging="1152"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="6">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo7"/>
			<w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="1296" w:hanging="1296"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="7">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo8"/>
			<w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7.%8"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="1440" w:hanging="1440"/>
			</w:pPr>
		</w:lvl>
		<w:lvl w:ilvl="8">
			<w:start w:val="1"/>
			<w:numFmt w:val="decimal"/>
			<w:pStyle w:val="Titolo9"/>
			<w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7.%8.%9"/>
			<w:lvlJc w:val="left"/>
			<w:pPr>
				<w:ind w:left="1584" w:hanging="1584"/>
			</w:pPr>
		</w:lvl>
	</w:abstractNum>
</xsl:variable>


<xsl:variable name="IndexOl" as="element()*">

<xsl:for-each select="(//ol[not(./ancestor::ol)][not(./ancestor::ul)])">

<w:num w:numId="{position() + 1 }">
	<w:abstractNumId w:val="{position()}"/>
</w:num>

</xsl:for-each>

</xsl:variable >


<xsl:variable name="IndexUl" as="element()*">

<xsl:for-each select="(//ul[not(./ancestor::ul)][not(./ancestor::ol)])">

<w:num w:numId="{position() + count(//ol[not(./ancestor::ol)][not(./ancestor::ul)]) +1 }">
	<w:abstractNumId w:val="{position() + count(//ol[not(./ancestor::ol)][not(./ancestor::ul)]) }"/>
</w:num>

</xsl:for-each>

</xsl:variable>


<xsl:variable name="AbstractOl"  as="element()*">

<xsl:for-each select="for $i in (1 to (count(//ol[not(./ancestor::ol)][not(./ancestor::ul)])  )) return $i" >

<w:abstractNum w:abstractNumId="{.}" >
	
	<w:multiLevelType w:val="hybridMultilevel"/>
	
	<w:lvl w:ilvl="0" >
		<w:start w:val="1"/>
		<w:numFmt w:val="decimal"/>
		<w:lvlText w:val="%1."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="720" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="1" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerLetter"/>
		<w:lvlText w:val="%2."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="1440" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="2" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerRoman"/>
		<w:lvlText w:val="%3."/>
		<w:lvlJc w:val="right"/>
		<w:pPr>
			<w:ind w:left="2160" w:hanging="180"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="3" >
		<w:start w:val="1"/>
		<w:numFmt w:val="decimal"/>
		<w:lvlText w:val="%4."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="2880" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="4" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerLetter"/>
		<w:lvlText w:val="%5."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="3600" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="5" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerRoman"/>
		<w:lvlText w:val="%6."/>
		<w:lvlJc w:val="right"/>
		<w:pPr>
			<w:ind w:left="4320" w:hanging="180"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="6" >
		<w:start w:val="1"/>
		<w:numFmt w:val="decimal"/>
		<w:lvlText w:val="%7."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="5040" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="7" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerLetter"/>
		<w:lvlText w:val="%8."/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="5760" w:hanging="360"/>
		</w:pPr>
	</w:lvl>
	<w:lvl w:ilvl="8" >
		<w:start w:val="1"/>
		<w:numFmt w:val="lowerRoman"/>
		<w:lvlText w:val="%9."/>
		<w:lvlJc w:val="right"/>
		<w:pPr>
			<w:ind w:left="6480" w:hanging="180"/>
		</w:pPr>
	</w:lvl>
</w:abstractNum>


</xsl:for-each>



</xsl:variable>





<xsl:variable name="AbstractUl"  as="element()*">

<xsl:for-each select="for $i in ( (count(//ol[not(./ancestor::ol)][not(./ancestor::ul)]) +1)  to (count(//ol[not(./ancestor::ol)][not(./ancestor::ul)]) + count(//ul[not(./ancestor::ul)][not(./ancestor::ol)])  )) return $i" >

<w:abstractNum w:abstractNumId="{.}" w15:restartNumberingAfterBreak="0">
	
	<w:multiLevelType w:val="hybridMultilevel"/>
	
	<w:lvl w:ilvl="0">
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="720" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="1" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val="o"/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="1440" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="2" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="2160" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="3" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="2880" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="4" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val="o"/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="3600" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="5" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="4320" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="6" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="5040" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="7" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val="o"/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="5760" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
		</w:rPr>
	</w:lvl>
	<w:lvl w:ilvl="8" >
		<w:start w:val="1"/>
		<w:numFmt w:val="bullet"/>
		<w:lvlText w:val=""/>
		<w:lvlJc w:val="left"/>
		<w:pPr>
			<w:ind w:left="6480" w:hanging="360"/>
		</w:pPr>
		<w:rPr>
			<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
		</w:rPr>
	</w:lvl>
</w:abstractNum>

</xsl:for-each>



</xsl:variable>

<w:numbering mc:Ignorable="w14 w15 w16se w16cid wp14">
	<xsl:copy-of select="$DefaultAbstract" />
	<xsl:copy-of select="$AbstractOl" />
	<xsl:copy-of select="$AbstractUl" />
	<xsl:copy-of select="$IndexOl" />
	<xsl:copy-of select="$IndexUl" />
	<xsl:copy-of select="$DefaultInd" />
</w:numbering>


</xsl:template>


</xsl:stylesheet>
