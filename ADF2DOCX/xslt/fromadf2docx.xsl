<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink" xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex" xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex" xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex" xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex" xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex" xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex" xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex" xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex" xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex" xmlns:f="http://my/function" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid" xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" mc:Ignorable="w14 w15 w16se wp14" xpath-default-namespace="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs f">
   <xsl:param name="dir" select="'./'" />
   <xsl:param name="content" as="xs:boolean" select="false()" />
   <xsl:variable name="links" select="doc(concat($dir, '/_rels/document.xml.rels'))" as="item()?" />
   <xsl:variable name="linksHeader" select="doc(concat($dir, '/_rels/header1.xml.rels'))" as="item()?" />
   <xsl:variable name="linksFooter" select="doc(concat($dir, '/_rels/footer1.xml.rels'))" as="item()?" />
   <xsl:variable name="numb" select="doc(concat($dir, '/numbering.xml'))" as="item()?" />
   <xsl:variable name="NumPag" as="element()*">
      <w:p>
         <w:pPr>
            <w:pStyle w:val="Intestazione" />
            <w:jc w:val="right" />
         </w:pPr>
         <w:r>
            <w:t>-</w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="begin" />
         </w:r>
         <w:r>
            <w:instrText xml:space="preserve"> PAGE   \* MERGEFORMAT </w:instrText>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="separate" />
         </w:r>
         <w:r w:rsidR="00EA3C8E">
            <w:rPr>
               <w:noProof />
            </w:rPr>
            <w:t>3</w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="end" />
         </w:r>
         <w:r>
            <w:t>/</w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="begin" />
         </w:r>
         <w:r>
            <w:instrText xml:space="preserve"> NUMPAGES   \* MERGEFORMAT </w:instrText>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="separate" />
         </w:r>
         <w:r w:rsidR="00EA3C8E">
            <w:rPr>
               <w:noProof />
            </w:rPr>
            <w:t>3</w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="end" />
         </w:r>
         <w:r>
            <w:t>-</w:t>
         </w:r>
      </w:p>
   </xsl:variable>
   <xsl:template match="/">
      <xsl:call-template name="add.header">
         <xsl:with-param name="head" as="element()?" select="//section[@id='layout-areas']/section[@id='header']" />
      </xsl:call-template>
      <xsl:call-template name="add.footer">
         <xsl:with-param name="head" as="element()?" select="//section[@id='layout-areas']/section[@id='footer']" />
      </xsl:call-template>
      <xsl:apply-templates />
   </xsl:template>
   <xsl:template match=" section[@role='doc-note2Author'] |section[@class='toc'] | section[@id='tot'] |  section[@id='tof'] | section[@id='toc'] | head | h1/span[@role='pdx-hcounter']  " />
   
   
   
   <!-- originale head | p//text()[ancestor::a]   |  th//text()[ancestor::a]-->
   <xsl:template match="body">
      <w:document mc:Ignorable="w14 w15 w16se w16cid wp14">
         <w:body>
            <xsl:if test="not($content)">
               <xsl:apply-templates select="section[@id='frontmatter']/child::*" />
               <w:p />
               <w:p>
                <w:r>
                 <w:br w:type="page" />
                </w:r>
               </w:p>
               <w:sdt>
                  <w:sdtPr>
                     <w:rPr>
                        <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman" />
                        <w:color w:val="auto" />
                        <w:sz w:val="22" />
                        <w:szCs w:val="22" />
                        <w:lang w:val="it-IT" />
                     </w:rPr>
                     <w:id w:val="-651286457" />
                     <w:docPartObj>
                        <w:docPartGallery w:val="Table of Contents" />
                        <w:docPartUnique />
                     </w:docPartObj>
                  </w:sdtPr>
                  <w:sdtEndPr>
                     <w:rPr>
                        <w:b />
                        <w:bCs />
                     </w:rPr>
                  </w:sdtEndPr>
                  <w:sdtContent>
                     <w:p>
                        <w:pPr>
                           <w:pStyle w:val="Titolosommario" />
                           <w:rPr>
                              <w:lang w:val="it-IT" />
                           </w:rPr>
                        </w:pPr>
                        <w:r>
                           <w:rPr>
                              <w:lang w:val="it-IT" />
                           </w:rPr>
                           <w:t>Sommario</w:t>
                        </w:r>
                     </w:p>
                     <w:p>
                        <w:r>
                           <w:fldChar w:fldCharType="begin" />
                        </w:r>
                        <w:r>
                           <w:instrText xml:space="preserve"> TOC \o "1-3" \h \z \u </w:instrText>
                        </w:r>
                        <w:r>
                           <w:fldChar w:fldCharType="separate" />
                        </w:r>
                        <w:r>
                           <w:rPr>
                              <w:rFonts w:hAnsiTheme="minorHAnsi" />
                              <w:b />
                              <w:bCs />
                              <w:noProof />
                           </w:rPr>
                           <w:t>Aggiorna il sommario cliccando qui, e in seguito, in alto, sul pulsante aggiorna sommario</w:t>
                        </w:r>
                        <w:r>
                           <w:rPr>
                              <w:b />
                              <w:bCs />
                           </w:rPr>
                           <w:fldChar w:fldCharType="end" />
                        </w:r>
                     </w:p>
                  </w:sdtContent>
               </w:sdt>
               <w:p />
               <w:p>
                  <w:pPr>
                     <w:pStyle w:val="Titolosommario" />
                  </w:pPr>
                  <w:r>
                     <w:t>Indice Figure</w:t>
                  </w:r>
               </w:p>
               <w:p>
                  <w:r>
                     <w:fldChar w:fldCharType="begin" />
                  </w:r>
                  <w:r>
                     <w:instrText xml:space="preserve"> TOC \h \z \c "Figura" </w:instrText>
                  </w:r>
                  <w:r>
                     <w:fldChar w:fldCharType="separate" />
                  </w:r>
                  <w:r>
                     <w:rPr>
                        <w:rFonts w:hAnsiTheme="minorHAnsi" />
                        <w:b />
                        <w:bCs />
                        <w:noProof />
                     </w:rPr>
                     <w:t>Aggiorna l'indice delle figure cliccando il tasto destro del mouse e scegliendo aggiorna campo</w:t>
                  </w:r>
                  <w:r>
                     <w:fldChar w:fldCharType="end" />
                  </w:r>
               </w:p>
               <w:p />
               <w:p>
                  <w:pPr>
                     <w:pStyle w:val="Titolosommario" />
                     <w:rPr>
                        <w:lang w:val="it-IT" />
                     </w:rPr>
                  </w:pPr>
                  <w:r w:rsidRPr="00782C9E">
                     <w:rPr>
                        <w:lang w:val="it-IT" />
                     </w:rPr>
                     <w:t>Indice Tabelle</w:t>
                  </w:r>
               </w:p>
               <w:p>
                  <w:r>
                     <w:fldChar w:fldCharType="begin" />
                  </w:r>
                  <w:r>
                     <w:instrText xml:space="preserve"> TOC \h \z \c "Tabella" </w:instrText>
                  </w:r>
                  <w:r>
                     <w:fldChar w:fldCharType="separate" />
                  </w:r>
                  <w:r>
                     <w:rPr>
                        <w:rFonts w:hAnsiTheme="minorHAnsi" />
                        <w:b />
                        <w:bCs />
                        <w:noProof />
                     </w:rPr>
                     <w:t>Aggiorna l'indice delle tabelle cliccando il tasto destro del mouse e scegliendo aggiorna campo</w:t>
                  </w:r>
                  <w:r>
                     <w:fldChar w:fldCharType="end" />
                  </w:r>
               </w:p>
               <w:p />
            </xsl:if>
            <xsl:call-template name="start">
               <xsl:with-param name="content" as="element()" select="section[@id='content']" />
            </xsl:call-template>
            <w:sectPr>
               <w:headerReference w:type="default" r:id="rId9" />
               <w:footerReference w:type="default" r:id="rId10" />
               <w:pgSz w:w="11906" w:h="16838" />
               <w:pgMar w:top="1417" w:right="1134" w:bottom="1134" w:left="1134" w:header="708" w:footer="708" w:gutter="0" />
               <w:cols w:space="708" />
               <w:titlePg />
               <w:docGrid w:linePitch="360" />
            </w:sectPr>
         </w:body>
      </w:document>
   </xsl:template>
   <xsl:template name="start">
      <xsl:param name="content" as="element()" />
      <xsl:apply-templates select="$content/child::section" />
   </xsl:template>
   <xsl:template match="section">
      <xsl:apply-templates />
   </xsl:template>
   
 
   
   <xsl:template match="h1">
      <xsl:variable name="level" as="xs:string" select="string(count(./ancestor::section[not(@id='content')][not(contains(@role,'pdx-non-hcountable'))]))" />
      <w:p>
         <w:pPr>
            <w:pStyle w:val="{concat('Titolo',$level)}" />
         </w:pPr>
         <xsl:if test="exists(./parent::section[@id])">
            <xsl:variable name="ident" as="xs:string" select="./parent::section/@id" />
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./parent::section/@id)" />
            <w:bookmarkStart w:id="{$PosId}" w:name="{$ident}" />
         </xsl:if>
         <!--
			<w:r>
				<w:t xml:space="preserve" ><xsl:value-of select="."/></w:t>
			</w:r> -->
         <xsl:apply-templates />
         <xsl:if test="exists(./parent::section[@id])">
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./parent::section/@id)" />
            <w:bookmarkEnd w:id="{$PosId}" />
         </xsl:if>
      </w:p>
   </xsl:template>
   <xsl:template match="p">
      <xsl:variable name="ListOl" as="xs:boolean" select="exists((./ancestor::ol))" />
      <xsl:variable name="ListUl" as="xs:boolean" select="exists((./ancestor::ul))" />
      <w:p>
         <xsl:choose>
            <xsl:when test="$ListOl and not($ListUl)">
               <xsl:variable name="ty" as="xs:string" select="('ol')" />
               <xsl:copy-of select="f:createPropertyP(.,$ty)" />
            </xsl:when>
            <xsl:when test="$ListUl and not($ListOl)">
               <xsl:variable name="ty" as="xs:string" select="('ul')" />
               <xsl:copy-of select="f:createPropertyP(.,$ty)" />
            </xsl:when>
            <!-- Caso liste discontinue-->
            <xsl:when test="$ListOl and $ListUl">
               <xsl:variable name="FirstOl" as="element()" select="(./ancestor::ol)[1]" />
               <xsl:variable name="FirstUl" as="element()" select="(./ancestor::ul)[1]" />
               <xsl:choose>
                  <!--il primo elemento di questa lista discontinua e' una lista ordinata -->
                  <xsl:when test="count($FirstOl intersect ($FirstUl/ancestor::ol)) = 1 ">
                     <xsl:variable name="ty" as="xs:string" select="('ol')" />
                     <xsl:copy-of select="f:createPropertyP(.,$ty)" />
                  </xsl:when>
                  <!--il primo elemento di questa lista discontinua e' una lista non ordinata -->
                  <xsl:when test="count($FirstUl intersect ($FirstOl/ancestor::ul)) = 1 ">
                     <xsl:variable name="ty" as="xs:string" select="('ul')" />
                     <xsl:copy-of select="f:createPropertyP(.,$ty)" />
                  </xsl:when>
                  <xsl:otherwise />
               </xsl:choose>
            </xsl:when>
            
            <xsl:when test="exists(./@data-style-msword)">
            
            	<xsl:choose>
            	
            	 <xsl:when test="contains(./@data-style-msword,'sse-main-title')">
            	 
            	 <w:pPr>
            	 
            	  <w:pStyle w:val="sse-main-title" />
            	 
            	 </w:pPr>
            	 
            	 </xsl:when>
            	
            	
            	 <xsl:otherwise/>
            	</xsl:choose>
            
            
            </xsl:when>
            
            <xsl:when test="exists(./ancestor::section[@id='header'])">
               <w:pPr>
                  <w:pStyle w:val="Intestazione" />
               </w:pPr>
            </xsl:when>
            <xsl:when test="exists(./ancestor::section[@id='footer'])">
               <w:pPr>
                  <w:pStyle w:val="PidiPagina" />
               </w:pPr>
            </xsl:when>
            <xsl:otherwise />
         </xsl:choose>
         <xsl:if test="exists(./@id)">
            <xsl:variable name="ident" as="xs:string" select="./@id" />
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./@id)" />
            <w:bookmarkStart w:id="{$PosId}" w:name="{$ident}" />
         </xsl:if>
         <!-- Questo segnalibro viene inserito per le righe delle tabelle -->
         <xsl:if test="exists(./parent::td/parent::tr[@id]) and empty(./parent::td/preceding-sibling::td) ">
            <xsl:variable name="ident" as="xs:string" select="./parent::td/parent::tr/@id" />
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./parent::td/parent::tr/@id)" />
            <w:bookmarkStart w:id="{$PosId}" w:name="{$ident}" w:colFirst="0" w:colLast="{count(./parent::td/following-sibling::td)}" />
         </xsl:if>
         
         <xsl:apply-templates />
         <xsl:if test="exists(./@id)">
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./@id)" />
            <w:bookmarkEnd w:id="{$PosId}" />
         </xsl:if>
      </w:p>
   </xsl:template>
   <xsl:template match="text()">
      <!-- originale p//text()[not(ancestor::a)] | th//text()[not(ancestor::a)] | h1//text() -->
      <xsl:if test=" normalize-space(.) != '' ">
         <w:r>
            <xsl:choose>
               <xsl:when test="exists(./parent::a[starts-with(@href,'http')])">
                  <w:rPr>
                     <w:rStyle w:val="Collegamentoipertestuale" />
                  </w:rPr>
               </xsl:when>
               <xsl:when test="exists(./parent::b)">
                  <w:rPr>
                     <w:b />
                  </w:rPr>
               </xsl:when>
               <xsl:when test="exists(./parent::i)">
                  <w:rPr>
                     <w:i />
                  </w:rPr>
               </xsl:when>
               <xsl:when test="exists(./parent::u)">
                  <w:rPr>
                     <w:u w:val="single" />
                  </w:rPr>
               </xsl:when>
               <xsl:when test="exists(./parent::sup)">
                  <w:rPr>
                     <w:vertAlign w:val="superscript" />
                  </w:rPr>
               </xsl:when>
               <xsl:when test="exists(./parent::sub)">
                  <w:rPr>
                     <w:vertAlign w:val="subscript" />
                  </w:rPr>
               </xsl:when>
               <xsl:otherwise />
            </xsl:choose>
            <w:t>
               <xsl:if test="empty(./ancestor::h1)">
                  <xsl:attribute name="xml:space">
                     <xsl:value-of select="('preserve')" />
                  </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="." />
            </w:t>
         </w:r>
      </xsl:if>
   </xsl:template>
   <xsl:template match="a[starts-with(@href,'http')]">
      <!-- Originale p/a-->
      <xsl:variable name="hr" as="xs:string" select="./@href" />
      <xsl:variable name="id" as="xs:string" select="f:getId($hr)" />
      <w:hyperlink r:id="{$id}">
         <!--	<w:r>
			<w:rPr>
				<w:rStyle w:val="Collegamentoipertestuale"/>
			</w:rPr> -->
         <!--
			<w:t> <xsl:value-of select="." /></w:t> -->
         <xsl:apply-templates />
         <!--	</w:r> -->
      </w:hyperlink>
   </xsl:template>
   <!-- Template per gestire riferimenti incrociati -->
   <xsl:template match="a[starts-with(@href,'#')][string-length(@href) &gt; 1 ]">
      <!-- Originale p/a-->
      <xsl:variable name="idReference" as="xs:string?" select="substring-after(./@href,'#')" />
      <xsl:variable name="instr" as="xs:string" select="concat(' REF ',$idReference,' \h')" />
      <w:r>
         <w:fldChar w:fldCharType="begin" />
      </w:r>
      <w:r>
         <w:instrText xml:space="preserve"><xsl:value-of select="$instr" /></w:instrText>
      </w:r>
      <w:r>
         <w:fldChar w:fldCharType="separate" />
      </w:r>
      <xsl:choose>
         <xsl:when test="normalize-space(string-join(.//text(),'')) = '' ">
            <w:r>
               <w:t xml:space="preserve">No text specified</w:t>
            </w:r>
         </xsl:when>
         <xsl:otherwise>
            <xsl:apply-templates />
         </xsl:otherwise>
      </xsl:choose>
      <w:r>
         <w:fldChar w:fldCharType="end" />
      </w:r>
   </xsl:template>
   <!-- Template per gestire tabelle (inizio) -->
   <xsl:template match="figure[@data-pdx-family='tables']">
      <xsl:apply-templates select="./child::table" />
      <xsl:if test="exists(.[not(ancestor::section[@id='frontmatter'])])">
         <xsl:call-template name="add.caption">
            <xsl:with-param name="type" as="xs:string" select="('Tabella')" />
            <xsl:with-param name="pos" as="xs:integer" select="f:getPositionTable(.)" />
         </xsl:call-template>
      </xsl:if>
   </xsl:template>
   <xsl:template match="table">
      <w:p />
      <w:tbl>
         <w:tblPr>
         
         	<xsl:choose>
         	
         		<xsl:when test="exists(./ancestor::section[@id='frontmatter'])">
         		<w:tblStyle w:val="Grigliatabella" />
         		</xsl:when>
         		
         		<xsl:otherwise>
         		<w:tblStyle w:val="Tabellagriglia4-colore1"/>
         		</xsl:otherwise>
         	
         	
         	</xsl:choose>
         
            
            <w:tblW w:w="100%" w:type="pct" />
            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="1" w:noVBand="1" />
         </w:tblPr>
         <xsl:apply-templates />
      </w:tbl>
      <w:p />
   </xsl:template>
   <xsl:template match="tr">
      <w:tr>
         <xsl:if test="empty(./preceding-sibling::tr) and empty(./ancestor::section[@id='frontmatter']) and empty(./ancestor::section[@id='layout-areas'])">
            <w:trPr>
               <w:tblHeader />
            </w:trPr>
         </xsl:if>
         <xsl:apply-templates />
         <xsl:if test="exists(./@id)">
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./@id)" />
            <w:bookmarkEnd w:id="{$PosId}" />
         </xsl:if>
      </w:tr>
   </xsl:template>
   <xsl:template match="th">
      <w:tc>
      
           <xsl:if test="exists(self::th[@colspan])">
         	<w:tcPr>
            <xsl:variable name="curVal" as="xs:integer" select="xs:integer(./@colspan)" />
            
            <xsl:if test="$curVal &gt;= 2 ">
               
                  <w:gridSpan w:val="{$curVal}" />
               
            </xsl:if>
            
           
            
            
            </w:tcPr>
         </xsl:if>
         <w:p>
         
          <!-- Questo segnalibro viene inserito per le righe delle tabelle che sono titoli -->
         <xsl:if test="exists(./parent::tr[@id]) and empty(./preceding-sibling::th) ">
            <xsl:variable name="ident" as="xs:string" select="./parent::tr/@id" />
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./parent::tr/@id)" />
            <w:bookmarkStart w:id="{$PosId}" w:name="{$ident}" w:colFirst="0" w:colLast="{count(./following-sibling::th)}" />
         </xsl:if>
         
            <!--	<w:r>
				<w:t><xsl:value-of select="."/></w:t>
			</w:r> -->
            <xsl:apply-templates />
         </w:p>
      </w:tc>
   </xsl:template>
   <!--	<xsl:template match="td[figure[@data-pdx-family='tables']]">
	
	<w:p>
	
		<xsl:apply-templates />
	
	</w:p>
	
	
	</xsl:template> -->
   <xsl:template match="td">
      <w:tc>
         <xsl:if test="exists(self::td[@colspan])">
         	<w:tcPr>
            <xsl:variable name="curVal" as="xs:integer" select="xs:integer(./@colspan)" />
            
            <xsl:if test="$curVal &gt;= 2 ">
               
                  <w:gridSpan w:val="{$curVal}" />
               
            </xsl:if>
            
           
            
            
            </w:tcPr>
         </xsl:if>
         <xsl:choose>
            <xsl:when test="count(./child::*) &gt; 0">
               <xsl:apply-templates />
            </xsl:when>
            <xsl:otherwise>
               <w:p />
            </xsl:otherwise>
         </xsl:choose>
         <xsl:if test="exists(self::td[figure[@data-pdx-family='tables']])">
            <w:p />
         </xsl:if>
      </w:tc>
   </xsl:template>
   <!-- Template per gestire immagini -->
   <xsl:template match="figure[@data-pdx-family='images']">
      <w:p>
         <!-- Per visualizzare le immagini al centro -->
         <w:pPr>
            <w:keepNext />
            <w:jc w:val="center" />
         </w:pPr>
         <xsl:apply-templates select="(./descendant::img[@src])" />
      </w:p>
      <!--
		<w:p>
			<w:pPr>
				<w:pStyle w:val="Didascalia"/>
			</w:pPr>
		<w:r>
			<w:t xml:space="preserve">Figura </w:t>
		</w:r>
		<w:r>
			<w:fldChar w:fldCharType="begin"/>
		</w:r>
		<w:r>
			<w:instrText xml:space="preserve"> SEQ Figura \* ARABIC </w:instrText>
		</w:r>
		<w:r>
			<w:fldChar w:fldCharType="separate"/>
		</w:r>
		<w:r>
			<w:rPr>
				<w:noProof/>
			</w:rPr>
			<w:t><xsl:value-of select="f:getPositionFigure(.)"/></w:t>
		</w:r>
		<w:r>
			<w:fldChar w:fldCharType="end"/>
		</w:r>
		<w:r>
			<w:t xml:space="preserve"><xsl:value-of select="string-join(.//figcaption/span[@role='pdx-incrementer'][@data-pdx-family='images']/following-sibling::text(),' ')"/></w:t>
		</w:r>
		</w:p> -->
      <xsl:if test="exists(.[not(ancestor::section[@id='frontmatter'])])">
         <xsl:call-template name="add.caption">
            <xsl:with-param name="type" as="xs:string" select="('Figura')" />
            <xsl:with-param name="pos" as="xs:integer" select="f:getPositionFigure(.)" />
         </xsl:call-template>
      </xsl:if>
   </xsl:template>
   <!-- Template per aggiungere le didascalie alle tabelle e alle figure -->
   <xsl:template name="add.caption">
      <xsl:param name="type" as="xs:string" />
      <xsl:param name="pos" as="xs:integer" />
      <xsl:variable name="PdxFamily" as="xs:string">
         <xsl:choose>
            <xsl:when test="$type eq 'Figura' ">
               <xsl:value-of select="('images')" />
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="('tables')" />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <w:p>
         <w:pPr>
            <w:pStyle w:val="Didascalia" />
            <!-- Posiziona il testo della didascalia al centro -->
            <xsl:if test=" $type eq 'Figura' ">
               <w:jc w:val="center" />
            </xsl:if>
         </w:pPr>
         <xsl:if test="exists(./@id)">
            <xsl:variable name="ident" as="xs:string" select="./@id" />
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./@id)" />
            <w:bookmarkStart w:id="{$PosId}" w:name="{$ident}" />
         </xsl:if>
         <w:r>
            <w:t xml:space="preserve"><xsl:value-of select="$type" /></w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="begin" />
         </w:r>
         <w:r>
            <w:instrText xml:space="preserve"><xsl:value-of select="concat('SEQ',' ',$type,' \* ARABIC')" /></w:instrText>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="separate" />
         </w:r>
         <w:r>
            <w:rPr>
               <w:noProof />
            </w:rPr>
            <w:t>
               <xsl:value-of select="$pos" />
            </w:t>
         </w:r>
         <w:r>
            <w:fldChar w:fldCharType="end" />
         </w:r>
         <xsl:if test="exists(./@id)">
            <xsl:variable name="PosId" as="xs:integer" select="f:getPosId(.,./@id)" />
            <w:bookmarkEnd w:id="{$PosId}" />
         </xsl:if>
         <w:r>
            <w:t xml:space="preserve"><xsl:value-of select="string-join(.//figcaption/span[@role='pdx-incrementer'][@data-pdx-family=$PdxFamily]/following-sibling::text(),' ')" /></w:t>
         </w:r>
      </w:p>
   </xsl:template>
   <xsl:template match="img[@src]">
      <xsl:variable name="NameImg" as="xs:string" select="(tokenize(./@src,'/'))[last()]" />
      <xsl:variable name="ImgNamD" as="xs:string" select="replace(normalize-space($NameImg),' ','')" />
      <xsl:variable name="Rid" as="xs:string" select="f:getIdImg(.,$ImgNamD)" />
      <xsl:variable name="pos" as="xs:integer" select="f:getPosImg(.)" />
      <w:r>
         <w:drawing>
            <wp:inline distT="0" distB="0" distL="0" distR="0">
            
            <xsl:variable name="altezza" as="xs:decimal">
            
            <xsl:choose>
            
            	<xsl:when test="exists(./@data-style-height-cm)">
            	
            	
            		<xsl:sequence select="xs:decimal(round(xs:decimal(./@data-style-height-cm * 360000)))"/>
            	
            	</xsl:when>
            	
            	<xsl:otherwise>
            	
            		
            	
            		<xsl:sequence select="xs:decimal('952500')"/>
            	
            	</xsl:otherwise>
            
            </xsl:choose>
            
            </xsl:variable>
            
            
             <xsl:variable name="larghezza" as="xs:decimal">
             
             <xsl:choose>
            
            	<xsl:when test="exists(./@data-style-width-cm)">
            	
            		<xsl:sequence select="xs:decimal(round(xs:decimal(./@data-style-width-cm * 360000)))"/>
            	
            	</xsl:when>
            	
            	<xsl:otherwise>
            	
            		<xsl:sequence select="xs:decimal('2076450')"/>
            	
            	</xsl:otherwise>
            	
            
             </xsl:choose>
            
            
            
            </xsl:variable>
            
            
             <wp:extent cx="{$larghezza}" cy="{$altezza}" />
         
               <wp:docPr id="{$pos}" name="{$ImgNamD}" />
               <wp:cNvGraphicFramePr>
                  <a:graphicFrameLocks noChangeAspect="1" />
               </wp:cNvGraphicFramePr>
               <a:graphic>
                  <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                     <pic:pic>
                        <pic:nvPicPr>
                           <pic:cNvPr id="{$pos}" name="{$ImgNamD}" />
                           <pic:cNvPicPr />
                        </pic:nvPicPr>
                        <pic:blipFill>
                           <a:blip r:embed="{$Rid}" cstate="print" />
                           <a:stretch>
                              <a:fillRect />
                           </a:stretch>
                        </pic:blipFill>
                        <pic:spPr>
                           <a:xfrm>
                              <a:off x="0" y="0" />
                          
                              <a:ext cx="{$larghezza}" cy="{$altezza}"/>
                           </a:xfrm>
                           <a:prstGeom prst="rect">
                              <a:avLst />
                           </a:prstGeom>
                        </pic:spPr>
                     </pic:pic>
                  </a:graphicData>
               </a:graphic>
            </wp:inline>
         </w:drawing>
      </w:r>
   </xsl:template>
   <!-- Template per creare header  -->
   <xsl:template name="add.header">
      <xsl:param name="head" as="element()?" />
      <xsl:result-document href="{concat($dir,'header1.xml')}">
         <w:hdr mc:Ignorable="w14 w15 w16se w16cid wp14">
            <xsl:choose>
               <xsl:when test="empty($head/child::*)">
                  <xsl:copy-of select="$NumPag" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:copy-of select="$NumPag" />
                  <xsl:apply-templates select="($head/child::*)" />
               </xsl:otherwise>
            </xsl:choose>
         </w:hdr>
      </xsl:result-document>
   </xsl:template>
   <!-- Template per creare footer  -->
   <xsl:template name="add.footer">
      <xsl:param name="head" as="element()?" />
      <xsl:result-document href="{concat($dir,'footer1.xml')}">
         <w:ftr mc:Ignorable="w14 w15 w16se w16cid wp14">
            <xsl:choose>
               <xsl:when test="empty($head/child::*)">
                  <w:p>
                     <w:pPr>
                        <w:pStyle w:val="Pidipagina" />
                     </w:pPr>
                  </w:p>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:apply-templates select="($head/child::*)" />
               </xsl:otherwise>
            </xsl:choose>
         </w:ftr>
      </xsl:result-document>
   </xsl:template>
   <!-- FUNZIONI -->
   <!-- Funzione per trovare la posizione (w:id) degli  elementi w:bookmarkStart  e w:bookmarkEnd    -->
   <xsl:function name="f:getPosId" as="xs:integer">
      <xsl:param name="curEl" as="element()" />
      <xsl:param name="curAttr" as="attribute()" />
      <xsl:variable name="allAttr" as="attribute()*" select="(root($curEl)//*/@id)" />
      <xsl:for-each select="$allAttr">
         <xsl:if test="generate-id($curAttr) = generate-id(.) ">
            <xsl:sequence select="position()" />
         </xsl:if>
      </xsl:for-each>
   </xsl:function>
   <!-- Funzione per trovare la posizione di un immagine dentro a figure per la numerazione -->
   <xsl:function name="f:getPositionFigure" as="xs:integer">
      <xsl:param name="currEl" as="element()" />
      <xsl:variable name="allImg" as="element()*" select="root($currEl)//section[@id='content']/descendant::figure[@data-pdx-family='images']" />
      <xsl:for-each select="$allImg">
         <xsl:if test=" generate-id($currEl) = generate-id(.) ">
            <xsl:sequence select="position()" />
         </xsl:if>
      </xsl:for-each>
   </xsl:function>
   <!-- Funzione per trovare la posizione di una tabella dentro a figure per la numerazione -->
   <xsl:function name="f:getPositionTable" as="xs:integer">
      <xsl:param name="currEl" as="element()" />
      <xsl:variable name="allImg" as="element()*" select="root($currEl)//section[@id='content']/descendant::figure[@data-pdx-family='tables']" />
      <xsl:for-each select="$allImg">
         <xsl:if test=" generate-id($currEl) = generate-id(.) ">
            <xsl:sequence select="position()" />
         </xsl:if>
      </xsl:for-each>
   </xsl:function>
   <xsl:function name="f:getPosImg" as="xs:integer">
      <xsl:param name="currEl" as="element()" />
      <xsl:variable name="allImg" as="element()*" select="root($currEl)/descendant::img[@src]" />
      <xsl:for-each select="$allImg">
         <xsl:if test=" generate-id($currEl) = generate-id(.) ">
            <xsl:sequence select="position()" />
         </xsl:if>
      </xsl:for-each>
   </xsl:function>
   <!-- Questa funzione serve per ricercare l'id dell' immagine-->
   <xsl:function name="f:getIdImg" as="xs:string" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
      <xsl:param name="curEl" as="element()" />
      <xsl:param name="key" as="xs:string" />
      <xsl:choose>
         <xsl:when test="f:insideHeader($curEl)">
            <xsl:variable name="ris" as="xs:string" select="($linksHeader//Relationship[ends-with(./@Target,$key)][not(./@TargetMode='External')]/@Id)[1]" />
            <xsl:sequence select="$ris" />
         </xsl:when>
         <xsl:when test="f:insideFooter($curEl)">
            <xsl:variable name="ris" as="xs:string" select="($linksFooter//Relationship[ends-with(./@Target,$key)][not(./@TargetMode='External')]/@Id)[1]" />
            <xsl:sequence select="$ris" />
         </xsl:when>
         <xsl:otherwise>
            <xsl:variable name="ris" as="xs:string" select="($links//Relationship[ends-with(./@Target,$key)][not(./@TargetMode='External')]/@Id)[1]" />
            <xsl:sequence select="$ris" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
   <!-- Questa funzione controlla se un elemento e' dentro la sezione header -->
   <xsl:function name="f:insideHeader" as="xs:boolean">
      <xsl:param name="curel" as="element()" />
      <xsl:choose>
         <xsl:when test="exists($curel/ancestor::section[@id='header'])">
            <xsl:sequence select="true()" />
         </xsl:when>
         <xsl:otherwise>
            <xsl:sequence select="false()" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
   <!-- Questa funzione controlla se un elemento e' dentro la sezione footer -->
   <xsl:function name="f:insideFooter" as="xs:boolean">
      <xsl:param name="curel" as="element()" />
      <xsl:choose>
         <xsl:when test="exists($curel/ancestor::section[@id='footer'])">
            <xsl:sequence select="true()" />
         </xsl:when>
         <xsl:otherwise>
            <xsl:sequence select="false()" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
   <!-- Questa funzione serve per ricercare l'id del collegamento ipertestuale -->
   <xsl:function name="f:getId" as="xs:string" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships">
      <xsl:param name="key" as="xs:string" />
      <xsl:variable name="ris" as="xs:string" select="($links//Relationship[@Target=$key]/@Id)[1]" />
      <xsl:sequence select="$ris" />
   </xsl:function>
   <!-- Questa funzione serve per definire il livello, espresso con un numero intero , che ha ogni elemento della lista  -->
   <xsl:function name="f:getIlvl" as="xs:integer">
      <xsl:param name="par" as="element()" />
      <xsl:param name="type" as="xs:string" />
      <xsl:sequence select=" count($par/ancestor::ul) + count($par/ancestor::ol) -1  " />
   </xsl:function>
   <!-- Questa funzione serve per definire l'id della lista -->
   <xsl:function name="f:getNumId" as="xs:integer">
      <xsl:param name="par" as="element()" />
      <xsl:param name="type" as="xs:string" />
      <xsl:choose>
         <xsl:when test="$type = 'ol' ">
            <xsl:variable name="LastAnc" as="element()" select="($par/ancestor::ol)[1]" />
            <xsl:variable name="ListOl" as="element()*" select="($par/ancestor::html//ol[not(ancestor::ol)][not(./ancestor::ul)])" />
            <xsl:for-each select="$ListOl">
               <xsl:if test="generate-id(.) = generate-id($LastAnc)">
                  <xsl:sequence select="position()+1" />
               </xsl:if>
            </xsl:for-each>
         </xsl:when>
         <xsl:otherwise>
            <xsl:variable name="LastAnc" as="element()" select="($par/ancestor::ul)[1]" />
            <xsl:variable name="ListUl" as="element()*" select="($par/ancestor::html//ul[not(ancestor::ul)][not(./ancestor::ol)])" />
            <xsl:for-each select="$ListUl">
               <xsl:if test="generate-id(.) = generate-id($LastAnc)">
                  <xsl:sequence select=" count(($par/ancestor::html//ol[not(ancestor::ol)][not(./ancestor::ul)])) + position()+1" />
               </xsl:if>
            </xsl:for-each>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:function>
   <!-- Funzione per trovare indentazione liste -->
   <xsl:function name="f:getIndentation" as="xs:integer">
      <xsl:param name="idL" as="xs:integer" />
      <xsl:param name="Level" as="xs:integer" />
      <xsl:variable name="AbstractId" select="$numb//w:num[@w:numId=$idL]/w:abstractNumId/@w:val" as="xs:integer" />
      <xsl:variable name="indent" as="xs:integer" select="$numb//w:abstractNum[@w:abstractNumId=$AbstractId]/w:lvl[@w:ilvl=$Level]/w:pPr/w:ind/@w:left" />
      <xsl:sequence select="$indent" />
   </xsl:function>
   <!-- Funzione per creare le proprieta' dei paragrafi delle liste -->
   <xsl:function name="f:createPropertyP" as="element()">
      <xsl:param name="curEl" as="element()" />
      <xsl:param name="tipo" as="xs:string" />
      <xsl:variable name="ilvl" as="xs:integer" select="f:getIlvl($curEl,$tipo)" />
      <xsl:variable name="numId" as="xs:integer" select="f:getNumId($curEl,$tipo)" />
      <xsl:variable name="ris" as="element()">
         <w:pPr>
            <xsl:choose>
               <xsl:when test="exists($curEl/ancestor::section[@id='header'])">
                  <w:pStyle w:val="Intestazione" />
               </xsl:when>
               <xsl:when test="exists($curEl/ancestor::section[@id='footer'])">
                  <w:pStyle w:val="Pidipagina" />
               </xsl:when>
               <xsl:otherwise>
                  <w:pStyle w:val="Paragrafoelenco" />
               </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
               <xsl:when test="not(exists($curEl/preceding-sibling::p))">
                  <w:numPr>
                     <w:ilvl w:val="{$ilvl}" />
                     <w:numId w:val="{$numId}" />
                  </w:numPr>
               </xsl:when>
               <xsl:when test="($ilvl &gt; 0)">
                  <xsl:variable name="indent" as="xs:integer" select="f:getIndentation($numId,$ilvl)" />
                  <w:ind w:left="{$indent}" />
               </xsl:when>
               <xsl:otherwise />
            </xsl:choose>
         </w:pPr>
      </xsl:variable>
      <xsl:sequence select="$ris" />
   </xsl:function>
</xsl:stylesheet>
