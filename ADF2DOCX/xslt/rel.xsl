<?xml version="1.0" encoding="UTF-8" standalone="yes"?>

<xsl:stylesheet xmlns="http://schemas.openxmlformats.org/package/2006/relationships" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
xmlns:f="http://my/XSLT/function"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xpath-default-namespace="http://www.w3.org/1999/xhtml"
exclude-result-prefixes="f xs ">
<xsl:param name="dir" select="'./'" />
<xsl:param name="content" as="xs:boolean" select="false()" />

<xsl:template match="/">

<xsl:call-template name="add.header.rel" />

<xsl:call-template name="add.footer.rel" />

<xsl:variable name="dr" as="element()*">

<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/><Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering" Target="numbering.xml"/><Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/><Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/><Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings" Target="webSettings.xml"/>
<Relationship Id="rId7" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes" Target="endnotes.xml"/>
<Relationship Id="rId8" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes" Target="footnotes.xml"/>
<Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/header" Target="header1.xml"/>
<Relationship Id="rId10" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer" Target="footer1.xml"/>
</xsl:variable>

<xsl:variable name="hl" as="element()*">

<xsl:choose>

<xsl:when test="$content">


<xsl:for-each select="(./descendant::section[@id='content']//a[starts-with(@href,'http')])" >
<xsl:variable name="id" as="xs:string" select="f:getId($dr,position())" />
<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink" Target="{@href}" 			    TargetMode="External" />
</xsl:for-each>

</xsl:when>

<xsl:otherwise>


<xsl:for-each select="(//a[starts-with(@href,'http')][empty(./ancestor::section[@id='layout-areas'])])" >
<xsl:variable name="id" as="xs:string" select="f:getId($dr,position())" />
<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink" Target="{@href}" 			    TargetMode="External" />
</xsl:for-each>


</xsl:otherwise>


</xsl:choose>

</xsl:variable>


<xsl:variable name="il" as="element()*">

<xsl:choose>

<xsl:when test="$content">

<xsl:variable name="numbHl" as="xs:integer" select="count((./descendant::section[@id='content']//a[starts-with(@href,'http')]))" />

<xsl:for-each select="(./descendant::section[@id='content']//img[@src])" >

<xsl:variable name="id" as="xs:string" select="f:getId($dr, $numbHl + position())" />
<xsl:variable name="ImgNam" as="xs:string" select="(tokenize(./@src,'/'))[last()]" />
<xsl:variable name="ImgNamD" as="xs:string" select="replace(normalize-space($ImgNam),' ','')" />
<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="{concat('media/',$ImgNamD)}"  />
</xsl:for-each>

</xsl:when>

<xsl:otherwise>


<xsl:variable name="numbHl" as="xs:integer" select="count((//a[starts-with(@href,'http')][empty(./ancestor::section[@id='layout-areas'])]))" />

<xsl:for-each select="(//img[@src][empty(./ancestor::section[@id='layout-areas'])])" >

<xsl:variable name="id" as="xs:string" select="f:getId($dr, $numbHl + position())" />
<xsl:variable name="ImgNam" as="xs:string" select="(tokenize(./@src,'/'))[last()]" />
<xsl:variable name="ImgNamD" as="xs:string" select="replace(normalize-space($ImgNam),' ','')" />
<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="{concat('media/',$ImgNamD)}"  />
</xsl:for-each>



</xsl:otherwise>

</xsl:choose>

</xsl:variable>




<Relationships>
<xsl:copy-of select="$dr" />
<xsl:copy-of select="$hl" />
<xsl:copy-of select="$il" />
</Relationships>
</xsl:template>



<xsl:template name="add.header.rel">

	<xsl:choose>
	
	<xsl:when test="exists(//section[@id='layout-areas']/section[@id='header']//img[@src])" >

	
		<xsl:result-document href="{concat($dir,'header1.xml.rels')}">
	
		<Relationships>
		
			<xsl:for-each select="(//section[@id='layout-areas']/section[@id='header']//img[@src])">
			
				<xsl:variable name="id" as="xs:string" select="concat('rId',string(position()))" />
				<xsl:variable name="ImgNam" as="xs:string" select="(tokenize(./@src,'/'))[last()]" />
				<xsl:variable name="ImgNamD" as="xs:string" select="replace(normalize-space($ImgNam),' ','')" />	
				<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="{concat('media/',$ImgNamD)}"  />
			</xsl:for-each>
	
		</Relationships>
		</xsl:result-document>
	
	</xsl:when>
	
	<xsl:otherwise>
	
	<xsl:result-document href="{concat($dir,'header1.xml.rels')}">
	
		<Relationships/>
		
	
	</xsl:result-document>
	
	
	</xsl:otherwise>
	
	
	</xsl:choose>


</xsl:template>



<xsl:template name="add.footer.rel">


	<xsl:choose>
	
	<xsl:when test="exists(//section[@id='layout-areas']/section[@id='footer']//img[@src])" >

	
		<xsl:result-document href="{concat($dir,'footer1.xml.rels')}">
	
		<Relationships>
			<xsl:for-each select="(//section[@id='layout-areas']/section[@id='footer']//img[@src])">
			
				<xsl:variable name="id" as="xs:string" select="concat('rId',string(position()))" />
				<xsl:variable name="ImgNam" as="xs:string" select="(tokenize(./@src,'/'))[last()]" />
				<xsl:variable name="ImgNamD" as="xs:string" select="replace(normalize-space($ImgNam),' ','')" />	
				<Relationship Id="{$id}" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="{concat('media/',$ImgNamD)}"  />
			</xsl:for-each>
	
		</Relationships>
		</xsl:result-document>
	
	</xsl:when>
	
	<xsl:otherwise>
	
	<xsl:result-document href="{concat($dir,'footer1.xml.rels')}">
	
		<Relationships/>
	
	
	</xsl:result-document>
	
	</xsl:otherwise>
	
	</xsl:choose>


</xsl:template>

<xsl:function name="f:getId" as="xs:string" xpath-default-namespace="http://schemas.openxmlformats.org/package/2006/relationships" >
<xsl:param name="p1" as="element()*" />
<xsl:param name="p2" as="xs:integer" />
<xsl:value-of select="concat('rId',string(count($p1[self::Relationship])  + $p2 ))" />
</xsl:function>




</xsl:stylesheet>
